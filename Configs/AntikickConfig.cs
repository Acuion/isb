﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ISP.Engine.Accounts;
using Newtonsoft.Json;

namespace ISP.Configs
{
    internal class AntikickAccountData
    {
        public string Name;
        public long? ChatId;
        

        public AntikickAccountData(string name, long? chatId = null)
        {
            Name = name;
            ChatId = chatId;
        }
    }

    internal sealed class AntikickTarget
    {
        public readonly string Name;

        public readonly List<AntikickAccountData> Accounts;

        public int IndexOfActiveAcc;

        [NonSerialized]
        public DateTime? WhenToRejoin;

        public AntikickTarget(string name)
        {
            IndexOfActiveAcc = 0;
            Name = name;
            Accounts = new List<AntikickAccountData>();
            WhenToRejoin = null;
        }
    }

    internal class AntikickConfig : ISBConfig
    {
        public readonly List<AntikickTarget> Targets;
        public int Delay, PartialRejoinDelay;
        [NonSerialized] public ConcurrentDictionary<string, List<long>> LeavedIntentionallyFrom;
        public AntikickConfig()
        {
            Targets = new List<AntikickTarget>();
            Delay = PartialRejoinDelay = 333;
        }

        public void Save()
        {
            File.WriteAllText("Configs\\antikickConfig.json", JsonConvert.SerializeObject(this));
        }

        public bool AddNewTarget(AntikickTarget atg)
        {
            lock (Targets)
            {
                if ((from x in Targets where x.Name == atg.Name select x).Any())
                    return false;
                Targets.Add(atg);
                return true;
            }
        }

        public void SetChatsList(IEnumerable<string> chats)
        {
            lock (Targets)
            {
                HashSet<string> inputNames = new HashSet<string>(chats);
                HashSet<string> existsingNames = new HashSet<string>(from t in Targets select t.Name);

                HashSet<string> toAdd = new HashSet<string>(inputNames);
                toAdd.ExceptWith(existsingNames);
                HashSet<string> toRemove = new HashSet<string>(existsingNames);
                toRemove.ExceptWith(inputNames);

                Targets.RemoveAll(x => toRemove.Contains(x.Name));
                foreach (string target in toAdd)
                    Targets.Add(new AntikickTarget(target));
            }
        }

        public void Validate()
        {
            lock (Targets)
            {
                foreach (var target in Targets)
                    target.Accounts.RemoveAll(x => !AccountsManager.GetAccountsList().Contains(x.Name));
            }
        }
    }
}