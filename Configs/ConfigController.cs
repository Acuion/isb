﻿using Newtonsoft.Json;
using System.IO;

namespace ISP.Configs
{
    internal static class ConfigController
    {
        public static InterfaceConfig InterfaceConfig;
        public static AnticapConfig AnticapConfig;

        public static TyperConfig TyperConfig;
        public static AntikickConfig AntikickConfig;

        public static void Load()
        {
            try
            {
                InterfaceConfig = JsonConvert.DeserializeObject<InterfaceConfig>(File.ReadAllText("Configs\\interfaceConfig.json"));
            }
            catch
            {
                InterfaceConfig = new InterfaceConfig();
            }
            try
            {
                TyperConfig = JsonConvert.DeserializeObject<TyperConfig>(File.ReadAllText("Configs\\typerConfig.json"));
            }
            catch
            {
                TyperConfig = new TyperConfig();
            }
            try
            {
                AnticapConfig = JsonConvert.DeserializeObject<AnticapConfig>(File.ReadAllText("Configs\\anticapConfig.json"));
            }
            catch
            {
                AnticapConfig = new AnticapConfig();
            }
            try
            {
                AntikickConfig = JsonConvert.DeserializeObject<AntikickConfig>(File.ReadAllText("Configs\\antikickConfig.json"));
            }
            catch
            {
                AntikickConfig = new AntikickConfig();
            }
        }
    }
}
