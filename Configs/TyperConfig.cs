﻿using Newtonsoft.Json;
using System.IO;

namespace ISP.Configs
{
    internal sealed class TyperConfig : ISBConfig
    {
        public int TypingDelay;
        public int SendingDelay;
        public string Name;
        public int NamePlacement;

        public TyperConfig()
        {
            TypingDelay = 100;
            SendingDelay = 1000;
            Name = "";
            NamePlacement = 0;
        }

        public void Save()
        {
            File.WriteAllText("Configs\\typerConfig.json", JsonConvert.SerializeObject(this));
        }
    }
}
