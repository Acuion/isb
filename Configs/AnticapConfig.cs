﻿using Newtonsoft.Json;
using System.IO;

namespace ISP.Configs
{
    internal enum SelectedMode
    {
        Rucaptcha,
        Anticaptcha,
        Manual
    };

    internal sealed class AnticapConfig : ISBConfig
    {
        public string RucaptchaKey;
        public string AnticaptchaKey;
        public SelectedMode SelectedMode;

        public AnticapConfig()
        {
            RucaptchaKey = AnticaptchaKey = "";
            SelectedMode = SelectedMode.Manual;
        }

        public void Save()
        {
            File.WriteAllText("Configs\\anticapConfig.json", JsonConvert.SerializeObject(this));
        }
    }
}
