﻿using Newtonsoft.Json;
using System.Drawing;
using System.IO;

namespace ISP.Configs
{
    internal sealed class InterfaceConfig : ISBConfig
    {
        public string PathToBackgroundImgGeneral;
        public string PathToBackgroundImgTabs;
        public int ForColorAsArgb;

        public InterfaceConfig()
        {
            PathToBackgroundImgGeneral = PathToBackgroundImgTabs = null;
            ForColorAsArgb = Color.Black.ToArgb();
        }

        public void Save()
        {
            File.WriteAllText("Configs\\interfaceConfig.json", JsonConvert.SerializeObject(this));
        }
    }
}
