﻿using System;
using System.Reflection;
using System.Windows.Forms;
using ISP.Forms;

[assembly: Obfuscation(Exclude = false, Feature = "namespace('ISP.Engine.Accounts'):-rename")]
[assembly: Obfuscation(Exclude = false, Feature = "namespace('ISP.Configs'):-rename")]
[assembly: Obfuscation(Exclude = false, Feature = "namespace('ISP.Tasks.Settings'):-rename")]

namespace ISP
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
