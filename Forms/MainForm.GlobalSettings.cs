﻿using System;
using ISP.Engine.Accounts;

namespace ISP.Forms
{
    internal partial class MainForm
    {
        private void LoadGlobalSettingsFormFromActive()
        {
            checkBox_flooderEnabled.Checked = AccountsManager.ActiveAccount.FlooderTaskSettings.Enabled;
            //checkBox_autoansEnabled.Checked = AccountsManager.ActiveAccount.;
            checkBox_chatsEnabled.Checked = AccountsManager.ActiveAccount.ChatsTaskSettings.Enabled;
            //checkBox_smartautoEnabled.Checked = aAccountsManager.ActiveAccount.;
            checkBox_inviterEnabled.Checked = AccountsManager.ActiveAccount.InviteTaskSettings.Enabled;
            //checkBox_remoteEnabled.Checked = AccountsManager.ActiveAccount.;

            numericUpDown_flooderDelay.Value = AccountsManager.ActiveAccount.FlooderTaskSettings.Delay;
            //numericUpDown_autoansDelay.Value = AccountsManager.ActiveAccount.;
            numericUpDown_chatsDelay.Value = AccountsManager.ActiveAccount.ChatsTaskSettings.Delay;
            //numericUpDown_smartautoDelay.Value = AccountsManager.ActiveAccount.;
            numericUpDown_inviterDelay.Value = AccountsManager.ActiveAccount.InviteTaskSettings.Delay;
            //numericUpDown_remoteDelay.Value = AccountsManager.ActiveAccount.;
        }

        private void CheckBox_flooderEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.FlooderTaskSettings.Enabled = checkBox_flooderEnabled.Checked);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void CheckBox_autoansEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void CheckBox_chatsEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.ChatsTaskSettings.Enabled = checkBox_chatsEnabled.Checked);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void CheckBox_smartautoEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void CheckBox_antikickEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.InviteTaskSettings.Enabled = checkBox_inviterEnabled.Checked);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void CheckBox_remoteEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void NumericUpDown_flooderDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.FlooderTaskSettings.Delay = (int)numericUpDown_flooderDelay.Value);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void NumericUpDown_autoansDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void NumericUpDown_chatsDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.ChatsTaskSettings.Delay = (int)numericUpDown_chatsDelay.Value);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void NumericUpDown_smartautoDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void NumericUpDown_antikickDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.InviteTaskSettings.Delay = (int)numericUpDown_inviterDelay.Value);
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
        }

        private void NumericUpDown_remoteDelay_ValueChanged(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
        }

        private void Button_resetSettings_Click(object sender, EventArgs e)
        {
            if (_notUserTriggeredChange)
                return;
            AccountsManager.ApplyToCurrent((a) => a.ResetSettings());
            AccountsManager.ApplyToCurrent((a) => a.SaveToDisk());
            ComboBox_accountsList_SelectedIndexChanged(null, null);
        }
    }
}
