﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ISP.Configs;

namespace ISP.Forms
{
    internal sealed partial class DesignForm : CenteredForm
    {
        public static MainForm FormMainForm;

        public DesignForm()
        {
            InitializeComponent();
            ForeColor = Color.FromArgb(ConfigController.InterfaceConfig.ForColorAsArgb);
            textBox_general.Text = ConfigController.InterfaceConfig.PathToBackgroundImgGeneral;
            textBox_tabs.Text = ConfigController.InterfaceConfig.PathToBackgroundImgTabs;
            pictureBox.BackColor = Color.FromArgb(ConfigController.InterfaceConfig.ForColorAsArgb);
        }

        private void button_fileFind_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            FormMainForm.SetBackPicture(openFileDialog.FileName);
            textBox_general.Text = openFileDialog.FileName;
        }

        private void button_fileFindTabs_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;
            FormMainForm.SetTabsBackPicture(openFileDialog.FileName);
            textBox_tabs.Text = openFileDialog.FileName;
        }

        private void button_selectColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK)
                return;
            FormMainForm.SetForeColor(colorDialog.Color);
            pictureBox.BackColor = colorDialog.Color;
        }

        private void button_resetImgGeneral_Click(object sender, EventArgs e)
        {
            FormMainForm.SetBackPicture(null);
            textBox_general.Text = "";
        }

        private void button_resetImgTabs_Click(object sender, EventArgs e)
        {
            FormMainForm.SetTabsBackPicture(null);
            textBox_tabs.Text = "";
        }

        private void button_resetColor_Click(object sender, EventArgs e)
        {
            FormMainForm.SetForeColor(Color.Black);
            pictureBox.BackColor = Color.Black;
        }
    }
}
