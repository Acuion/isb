﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ISP.Forms
{
    partial class DesignForm
    {
        private IContainer components = null;
        private TextBox textBox_general;
        private Button button_fileFindGeneral;
        private Label label1;
        private Button button_resetImgGeneral;
        private ColorDialog colorDialog;
        private OpenFileDialog openFileDialog;
        private Button button_resetImgTabs;
        private Label label2;
        private Button button_fileFindTabs;
        private TextBox textBox_tabs;
        private PictureBox pictureBox;
        private Label label3;
        private Button button_resetColor;
        private Button button_selectColor;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            textBox_general = new TextBox();
            button_fileFindGeneral = new Button();
            label1 = new Label();
            button_resetImgGeneral = new Button();
            colorDialog = new ColorDialog();
            openFileDialog = new OpenFileDialog();
            button_resetImgTabs = new Button();
            label2 = new Label();
            button_fileFindTabs = new Button();
            textBox_tabs = new TextBox();
            pictureBox = new PictureBox();
            label3 = new Label();
            button_resetColor = new Button();
            button_selectColor = new Button();
            ((ISupportInitialize)pictureBox).BeginInit();
            SuspendLayout();
            textBox_general.Location = new Point(9, 22);
            textBox_general.Name = "textBox_general";
            textBox_general.ReadOnly = true;
            textBox_general.Size = new Size(305, 20);
            textBox_general.TabIndex = 0;
            button_fileFindGeneral.FlatStyle = FlatStyle.Flat;
            button_fileFindGeneral.Location = new Point(320, 19);
            button_fileFindGeneral.Name = "button_fileFindGeneral";
            button_fileFindGeneral.Size = new Size(27, 23);
            button_fileFindGeneral.TabIndex = 1;
            button_fileFindGeneral.Text = "...";
            button_fileFindGeneral.UseVisualStyleBackColor = true;
            button_fileFindGeneral.Click += new EventHandler(button_fileFind_Click);
            label1.AutoSize = true;
            label1.Location = new Point(109, 6);
            label1.Name = "label1";
            label1.Size = new Size(125, 13);
            label1.TabIndex = 2;
            label1.Text = "Фоновое изображение";
            button_resetImgGeneral.FlatStyle = FlatStyle.Flat;
            button_resetImgGeneral.Location = new Point(351, 19);
            button_resetImgGeneral.Name = "button_resetImgGeneral";
            button_resetImgGeneral.Size = new Size(27, 23);
            button_resetImgGeneral.TabIndex = 3;
            button_resetImgGeneral.Text = "Х";
            button_resetImgGeneral.UseVisualStyleBackColor = true;
            button_resetImgGeneral.Click += new EventHandler(button_resetImgGeneral_Click);
            openFileDialog.FileName = "openFileDialog";
            button_resetImgTabs.FlatStyle = FlatStyle.Flat;
            button_resetImgTabs.Location = new Point(351, 58);
            button_resetImgTabs.Name = "button_resetImgTabs";
            button_resetImgTabs.Size = new Size(27, 23);
            button_resetImgTabs.TabIndex = 7;
            button_resetImgTabs.Text = "Х";
            button_resetImgTabs.UseVisualStyleBackColor = true;
            button_resetImgTabs.Click += new EventHandler(button_resetImgTabs_Click);
            label2.AutoSize = true;
            label2.Location = new Point(83, 45);
            label2.Name = "label2";
            label2.Size = new Size(190, 13);
            label2.TabIndex = 6;
            label2.Text = "Фоновое изображение на вкладках";
            button_fileFindTabs.FlatStyle = FlatStyle.Flat;
            button_fileFindTabs.Location = new Point(320, 58);
            button_fileFindTabs.Name = "button_fileFindTabs";
            button_fileFindTabs.Size = new Size(27, 23);
            button_fileFindTabs.TabIndex = 5;
            button_fileFindTabs.Text = "...";
            button_fileFindTabs.UseVisualStyleBackColor = true;
            button_fileFindTabs.Click += new EventHandler(button_fileFindTabs_Click);
            textBox_tabs.Location = new Point(9, 61);
            textBox_tabs.Name = "textBox_tabs";
            textBox_tabs.ReadOnly = true;
            textBox_tabs.Size = new Size(305, 20);
            textBox_tabs.TabIndex = 4;
            pictureBox.BackColor = SystemColors.WindowText;
            pictureBox.Location = new Point(9, 99);
            pictureBox.Name = "pictureBox";
            pictureBox.Size = new Size(305, 23);
            pictureBox.TabIndex = 8;
            pictureBox.TabStop = false;
            label3.AutoSize = true;
            label3.Location = new Point(135, 84);
            label3.Name = "label3";
            label3.Size = new Size(69, 13);
            label3.TabIndex = 9;
            label3.Text = "Цвет текста";
            button_resetColor.FlatStyle = FlatStyle.Flat;
            button_resetColor.Location = new Point(351, 99);
            button_resetColor.Name = "button_resetColor";
            button_resetColor.Size = new Size(27, 23);
            button_resetColor.TabIndex = 10;
            button_resetColor.Text = "Х";
            button_resetColor.UseVisualStyleBackColor = true;
            button_resetColor.Click += new EventHandler(button_resetColor_Click);
            button_selectColor.FlatStyle = FlatStyle.Flat;
            button_selectColor.Location = new Point(318, 99);
            button_selectColor.Name = "button_selectColor";
            button_selectColor.Size = new Size(27, 23);
            button_selectColor.TabIndex = 11;
            button_selectColor.Text = "...";
            button_selectColor.UseVisualStyleBackColor = true;
            button_selectColor.Click += new EventHandler(button_selectColor_Click);
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.WindowFrame;
            ClientSize = new Size(385, 128);
            Controls.Add((Control)button_selectColor);
            Controls.Add((Control)button_resetColor);
            Controls.Add((Control)label3);
            Controls.Add((Control)pictureBox);
            Controls.Add((Control)button_resetImgTabs);
            Controls.Add((Control)label2);
            Controls.Add((Control)button_fileFindTabs);
            Controls.Add((Control)textBox_tabs);
            Controls.Add((Control)button_resetImgGeneral);
            Controls.Add((Control)label1);
            Controls.Add((Control)button_fileFindGeneral);
            Controls.Add((Control)textBox_general);
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            Name = "DesignForm";
            Text = "ISB :: Design";
            ((ISupportInitialize)pictureBox).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }
    }
}
