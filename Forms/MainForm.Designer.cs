﻿using System.ComponentModel;
using System.Windows.Forms;

namespace ISP.Forms
{
    partial class MainForm
    {
        private IContainer components = null;
        private TabControl tabControl;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private TabPage tabPage7;
        private TabPage tabPage8;
        private MenuStrip menuStrip;
        private DateTimePicker dateTimePicker_timeToStop;
        private JCS.ToggleSwitch checkBox_shutownOnTime;
        private Button button_startStop;
        private ComboBox comboBox_accountsList;
        private Label label2;
        private Button button_applyToAll;
        private Button button_anticaptchaKeys;
        private Button button_design;
        private TabPage tabPage9;
        private PictureBox pictureBox_captPic;
        private TextBox textBox_manualCaptchaAnswer;
        private RadioButton radioButton_captchaManual;
        private RadioButton radioButton_captchaRucap;
        private RadioButton radioButton_captchaAnticap;
        private Label label1;
        private Label label5;
        private NumericUpDown numericUpDown_remoteDelay;
        private Label label17;
        private JCS.ToggleSwitch checkBox_remoteEnabled;
        private NumericUpDown numericUpDown_inviterDelay;
        private Label label15;
        private JCS.ToggleSwitch checkBox_inviterEnabled;
        private NumericUpDown numericUpDown_smartautoDelay;
        private NumericUpDown numericUpDown_chatsDelay;
        private NumericUpDown numericUpDown_autoansDelay;
        private Label label11;
        private JCS.ToggleSwitch checkBox_smartautoEnabled;
        private Label label9;
        private JCS.ToggleSwitch checkBox_chatsEnabled;
        private Label label7;
        private JCS.ToggleSwitch checkBox_autoansEnabled;
        private Label label6;
        private JCS.ToggleSwitch checkBox_flooderEnabled;
        private NumericUpDown numericUpDown_flooderDelay;
        private Button button_otherBots;
        private ToolStripMenuItem toolStripMenuItem_log;
        private DataGridView dataGridView_flooderView;
        private DataGridView dataGridView_flooderEditable;
        private DataGridView dataGridView_autoansEditable;
        private DataGridView dataGridView_autoansView;
        private DataGridView dataGridView_chatsView;
        private DataGridView dataGridView_chatsEditable;
        private Label label21;
        private TextBox textBox5;
        private ToolStripMenuItem toolStripMenuItem_help;
        private Label label22;
        private RichTextBox richTextBox1;
        private Button button_resetSettings;
        private Label label19;
        private Label label_forAll;
        private Label label24;
        private Label label25;
        private Label label26;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.button_resetSettings = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_remoteDelay = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.checkBox_remoteEnabled = new JCS.ToggleSwitch();
            this.numericUpDown_inviterDelay = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.checkBox_inviterEnabled = new JCS.ToggleSwitch();
            this.numericUpDown_smartautoDelay = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_chatsDelay = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_autoansDelay = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBox_smartautoEnabled = new JCS.ToggleSwitch();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox_chatsEnabled = new JCS.ToggleSwitch();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox_autoansEnabled = new JCS.ToggleSwitch();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox_flooderEnabled = new JCS.ToggleSwitch();
            this.numericUpDown_flooderDelay = new System.Windows.Forms.NumericUpDown();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBox_flooderStickerId = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox_flooderStickerFile = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBox_flooderPhrasesWithDotsSource = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBox_flooderPhrasesSource = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_flooderPictureSource = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_flooderChange = new System.Windows.Forms.Button();
            this.button_flooderRemove = new System.Windows.Forms.Button();
            this.button_flooderAdd = new System.Windows.Forms.Button();
            this.dataGridView_flooderEditable = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridView_flooderView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.dataGridView_autoansEditable = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridView_autoansView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button_chatsChange = new System.Windows.Forms.Button();
            this.button_chatsRemove = new System.Windows.Forms.Button();
            this.button_chatsAdd = new System.Windows.Forms.Button();
            this.dataGridView_chatsEditable = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridView_chatsView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox_yandexKeyVO = new System.Windows.Forms.TextBox();
            this.button_sendVO = new System.Windows.Forms.Button();
            this.numericUpDown_speedVO = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_emotionVO = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox_voiceVO = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_targetVO = new System.Windows.Forms.TextBox();
            this.richTextBox_messageVO = new System.Windows.Forms.RichTextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem_log = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_manager = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_help = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_reload = new System.Windows.Forms.ToolStripMenuItem();
            this.dateTimePicker_timeToStop = new System.Windows.Forms.DateTimePicker();
            this.checkBox_shutownOnTime = new JCS.ToggleSwitch();
            this.comboBox_accountsList = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_manualCaptchaAnswer = new System.Windows.Forms.TextBox();
            this.radioButton_captchaManual = new System.Windows.Forms.RadioButton();
            this.radioButton_captchaRucap = new System.Windows.Forms.RadioButton();
            this.radioButton_captchaAnticap = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label_forAll = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.timer_manualCaptcha = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.timer_shutown = new System.Windows.Forms.Timer(this.components);
            this.label37 = new System.Windows.Forms.Label();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button_otherBots = new System.Windows.Forms.Button();
            this.button_design = new System.Windows.Forms.Button();
            this.button_anticaptchaKeys = new System.Windows.Forms.Button();
            this.button_applyToAll = new System.Windows.Forms.Button();
            this.button_startStop = new System.Windows.Forms.Button();
            this.pictureBox_captPic = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip_otherBots = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_typerBot = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_antikickBot = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_remoteDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_inviterDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_smartautoDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_chatsDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_autoansDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_flooderDelay)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_flooderEditable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_flooderView)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_autoansEditable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_autoansView)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_chatsEditable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_chatsView)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_speedVO)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_captPic)).BeginInit();
            this.contextMenuStrip_otherBots.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage9);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage7);
            this.tabControl.Controls.Add(this.tabPage8);
            this.tabControl.Enabled = false;
            this.tabControl.Location = new System.Drawing.Point(1, 114);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(694, 426);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage9.Controls.Add(this.label36);
            this.tabPage9.Controls.Add(this.label35);
            this.tabPage9.Controls.Add(this.label33);
            this.tabPage9.Controls.Add(this.label32);
            this.tabPage9.Controls.Add(this.label31);
            this.tabPage9.Controls.Add(this.label30);
            this.tabPage9.Controls.Add(this.button_resetSettings);
            this.tabPage9.Controls.Add(this.label5);
            this.tabPage9.Controls.Add(this.numericUpDown_remoteDelay);
            this.tabPage9.Controls.Add(this.label17);
            this.tabPage9.Controls.Add(this.checkBox_remoteEnabled);
            this.tabPage9.Controls.Add(this.numericUpDown_inviterDelay);
            this.tabPage9.Controls.Add(this.label15);
            this.tabPage9.Controls.Add(this.checkBox_inviterEnabled);
            this.tabPage9.Controls.Add(this.numericUpDown_smartautoDelay);
            this.tabPage9.Controls.Add(this.numericUpDown_chatsDelay);
            this.tabPage9.Controls.Add(this.numericUpDown_autoansDelay);
            this.tabPage9.Controls.Add(this.label11);
            this.tabPage9.Controls.Add(this.checkBox_smartautoEnabled);
            this.tabPage9.Controls.Add(this.label9);
            this.tabPage9.Controls.Add(this.checkBox_chatsEnabled);
            this.tabPage9.Controls.Add(this.label7);
            this.tabPage9.Controls.Add(this.checkBox_autoansEnabled);
            this.tabPage9.Controls.Add(this.label6);
            this.tabPage9.Controls.Add(this.checkBox_flooderEnabled);
            this.tabPage9.Controls.Add(this.numericUpDown_flooderDelay);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(686, 400);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Настройка";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Location = new System.Drawing.Point(215, 237);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(125, 13);
            this.label36.TabIndex = 39;
            this.label36.Text = "Удалённое управление";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Location = new System.Drawing.Point(215, 214);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 13);
            this.label35.TabIndex = 38;
            this.label35.Text = "Инвайтер";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(215, 191);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(114, 13);
            this.label33.TabIndex = 36;
            this.label33.Text = "Умный автоответчик";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(215, 168);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(46, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Беседы";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(215, 145);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(76, 13);
            this.label31.TabIndex = 34;
            this.label31.Text = "Автоответчик";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(215, 122);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 13);
            this.label30.TabIndex = 33;
            this.label30.Text = "Флудер";
            // 
            // button_resetSettings
            // 
            this.button_resetSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_resetSettings.Location = new System.Drawing.Point(159, 260);
            this.button_resetSettings.Name = "button_resetSettings";
            this.button_resetSettings.Size = new System.Drawing.Size(344, 23);
            this.button_resetSettings.TabIndex = 32;
            this.button_resetSettings.Text = "Стереть все настройки в этом аккаунте";
            this.button_resetSettings.UseVisualStyleBackColor = true;
            this.button_resetSettings.Click += new System.EventHandler(this.Button_resetSettings_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(365, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "интервал работы";
            // 
            // numericUpDown_remoteDelay
            // 
            this.numericUpDown_remoteDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_remoteDelay.Location = new System.Drawing.Point(356, 234);
            this.numericUpDown_remoteDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_remoteDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_remoteDelay.Name = "numericUpDown_remoteDelay";
            this.numericUpDown_remoteDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_remoteDelay.TabIndex = 30;
            this.numericUpDown_remoteDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_remoteDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_remoteDelay_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(482, 239);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 29;
            this.label17.Text = "мс";
            // 
            // checkBox_remoteEnabled
            // 
            this.checkBox_remoteEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_remoteEnabled.Location = new System.Drawing.Point(167, 235);
            this.checkBox_remoteEnabled.Name = "checkBox_remoteEnabled";
            this.checkBox_remoteEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_remoteEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_remoteEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_remoteEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_remoteEnabled.TabIndex = 27;
            this.checkBox_remoteEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_remoteEnabled_CheckedChanged);
            // 
            // numericUpDown_inviterDelay
            // 
            this.numericUpDown_inviterDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_inviterDelay.Location = new System.Drawing.Point(356, 211);
            this.numericUpDown_inviterDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_inviterDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_inviterDelay.Name = "numericUpDown_inviterDelay";
            this.numericUpDown_inviterDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_inviterDelay.TabIndex = 26;
            this.numericUpDown_inviterDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_inviterDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_antikickDelay_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(482, 216);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "мс";
            // 
            // checkBox_inviterEnabled
            // 
            this.checkBox_inviterEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_inviterEnabled.Location = new System.Drawing.Point(167, 212);
            this.checkBox_inviterEnabled.Name = "checkBox_inviterEnabled";
            this.checkBox_inviterEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_inviterEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_inviterEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_inviterEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_inviterEnabled.TabIndex = 23;
            this.checkBox_inviterEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_antikickEnabled_CheckedChanged);
            // 
            // numericUpDown_smartautoDelay
            // 
            this.numericUpDown_smartautoDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_smartautoDelay.Location = new System.Drawing.Point(356, 188);
            this.numericUpDown_smartautoDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_smartautoDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_smartautoDelay.Name = "numericUpDown_smartautoDelay";
            this.numericUpDown_smartautoDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_smartautoDelay.TabIndex = 18;
            this.numericUpDown_smartautoDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_smartautoDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_smartautoDelay_ValueChanged);
            // 
            // numericUpDown_chatsDelay
            // 
            this.numericUpDown_chatsDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_chatsDelay.Location = new System.Drawing.Point(356, 165);
            this.numericUpDown_chatsDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_chatsDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_chatsDelay.Name = "numericUpDown_chatsDelay";
            this.numericUpDown_chatsDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_chatsDelay.TabIndex = 17;
            this.numericUpDown_chatsDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_chatsDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_chatsDelay_ValueChanged);
            // 
            // numericUpDown_autoansDelay
            // 
            this.numericUpDown_autoansDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_autoansDelay.Location = new System.Drawing.Point(356, 142);
            this.numericUpDown_autoansDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_autoansDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_autoansDelay.Name = "numericUpDown_autoansDelay";
            this.numericUpDown_autoansDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_autoansDelay.TabIndex = 16;
            this.numericUpDown_autoansDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_autoansDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_autoansDelay_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(482, 193);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "мс";
            // 
            // checkBox_smartautoEnabled
            // 
            this.checkBox_smartautoEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_smartautoEnabled.Location = new System.Drawing.Point(167, 189);
            this.checkBox_smartautoEnabled.Name = "checkBox_smartautoEnabled";
            this.checkBox_smartautoEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_smartautoEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_smartautoEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_smartautoEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_smartautoEnabled.TabIndex = 13;
            this.checkBox_smartautoEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_smartautoEnabled_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(482, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "мс";
            // 
            // checkBox_chatsEnabled
            // 
            this.checkBox_chatsEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_chatsEnabled.Location = new System.Drawing.Point(167, 166);
            this.checkBox_chatsEnabled.Name = "checkBox_chatsEnabled";
            this.checkBox_chatsEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_chatsEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_chatsEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_chatsEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_chatsEnabled.TabIndex = 9;
            this.checkBox_chatsEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_chatsEnabled_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(482, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "мс";
            // 
            // checkBox_autoansEnabled
            // 
            this.checkBox_autoansEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_autoansEnabled.Location = new System.Drawing.Point(167, 143);
            this.checkBox_autoansEnabled.Name = "checkBox_autoansEnabled";
            this.checkBox_autoansEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_autoansEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_autoansEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_autoansEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_autoansEnabled.TabIndex = 5;
            this.checkBox_autoansEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_autoansEnabled_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(482, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "мс";
            // 
            // checkBox_flooderEnabled
            // 
            this.checkBox_flooderEnabled.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_flooderEnabled.Location = new System.Drawing.Point(167, 120);
            this.checkBox_flooderEnabled.Name = "checkBox_flooderEnabled";
            this.checkBox_flooderEnabled.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_flooderEnabled.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_flooderEnabled.Size = new System.Drawing.Size(45, 19);
            this.checkBox_flooderEnabled.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_flooderEnabled.TabIndex = 1;
            this.checkBox_flooderEnabled.CheckedChanged += new JCS.ToggleSwitch.CheckedChangedDelegate(this.CheckBox_flooderEnabled_CheckedChanged);
            // 
            // numericUpDown_flooderDelay
            // 
            this.numericUpDown_flooderDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_flooderDelay.Location = new System.Drawing.Point(356, 119);
            this.numericUpDown_flooderDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_flooderDelay.Minimum = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_flooderDelay.Name = "numericUpDown_flooderDelay";
            this.numericUpDown_flooderDelay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_flooderDelay.TabIndex = 0;
            this.numericUpDown_flooderDelay.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
            this.numericUpDown_flooderDelay.ValueChanged += new System.EventHandler(this.NumericUpDown_flooderDelay_ValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Controls.Add(this.comboBox_flooderStickerId);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.comboBox_flooderStickerFile);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.comboBox_flooderPhrasesWithDotsSource);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.comboBox_flooderPhrasesSource);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.comboBox_flooderPictureSource);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.button_flooderChange);
            this.tabPage1.Controls.Add(this.button_flooderRemove);
            this.tabPage1.Controls.Add(this.button_flooderAdd);
            this.tabPage1.Controls.Add(this.dataGridView_flooderEditable);
            this.tabPage1.Controls.Add(this.dataGridView_flooderView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(686, 400);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Флудер";
            // 
            // comboBox_flooderStickerId
            // 
            this.comboBox_flooderStickerId.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_flooderStickerId.FormattingEnabled = true;
            this.comboBox_flooderStickerId.Location = new System.Drawing.Point(579, 19);
            this.comboBox_flooderStickerId.Name = "comboBox_flooderStickerId";
            this.comboBox_flooderStickerId.Size = new System.Drawing.Size(90, 21);
            this.comboBox_flooderStickerId.TabIndex = 32;
            this.comboBox_flooderStickerId.SelectedIndexChanged += new System.EventHandler(this.ComboBox_flooderStickerId_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(596, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 13);
            this.label29.TabIndex = 31;
            this.label29.Text = "Стикер id";
            // 
            // comboBox_flooderStickerFile
            // 
            this.comboBox_flooderStickerFile.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_flooderStickerFile.FormattingEnabled = true;
            this.comboBox_flooderStickerFile.Location = new System.Drawing.Point(451, 19);
            this.comboBox_flooderStickerFile.Name = "comboBox_flooderStickerFile";
            this.comboBox_flooderStickerFile.Size = new System.Drawing.Size(122, 21);
            this.comboBox_flooderStickerFile.TabIndex = 30;
            this.comboBox_flooderStickerFile.SelectedIndexChanged += new System.EventHandler(this.ComboBox_flooderStickerFile_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(458, 3);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 13);
            this.label28.TabIndex = 29;
            this.label28.Text = "Файл со стикерами";
            // 
            // comboBox_flooderPhrasesWithDotsSource
            // 
            this.comboBox_flooderPhrasesWithDotsSource.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_flooderPhrasesWithDotsSource.FormattingEnabled = true;
            this.comboBox_flooderPhrasesWithDotsSource.Location = new System.Drawing.Point(293, 19);
            this.comboBox_flooderPhrasesWithDotsSource.Name = "comboBox_flooderPhrasesWithDotsSource";
            this.comboBox_flooderPhrasesWithDotsSource.Size = new System.Drawing.Size(152, 21);
            this.comboBox_flooderPhrasesWithDotsSource.TabIndex = 28;
            this.comboBox_flooderPhrasesWithDotsSource.SelectedIndexChanged += new System.EventHandler(this.ComboBox_flooderPhrasesWithDotsSource_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(317, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "Источник фраз с ...";
            // 
            // comboBox_flooderPhrasesSource
            // 
            this.comboBox_flooderPhrasesSource.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_flooderPhrasesSource.FormattingEnabled = true;
            this.comboBox_flooderPhrasesSource.Location = new System.Drawing.Point(134, 19);
            this.comboBox_flooderPhrasesSource.Name = "comboBox_flooderPhrasesSource";
            this.comboBox_flooderPhrasesSource.Size = new System.Drawing.Size(152, 21);
            this.comboBox_flooderPhrasesSource.TabIndex = 26;
            this.comboBox_flooderPhrasesSource.SelectedIndexChanged += new System.EventHandler(this.ComboBox_flooderPhrasesSource_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(168, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Источник фраз";
            // 
            // comboBox_flooderPictureSource
            // 
            this.comboBox_flooderPictureSource.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_flooderPictureSource.FormattingEnabled = true;
            this.comboBox_flooderPictureSource.Items.AddRange(new object[] {
            "images.txt (общий)",
            "Папка Upload\\Images"});
            this.comboBox_flooderPictureSource.Location = new System.Drawing.Point(7, 19);
            this.comboBox_flooderPictureSource.Name = "comboBox_flooderPictureSource";
            this.comboBox_flooderPictureSource.Size = new System.Drawing.Size(121, 21);
            this.comboBox_flooderPictureSource.TabIndex = 24;
            this.comboBox_flooderPictureSource.Text = "images.txt (общий)";
            this.comboBox_flooderPictureSource.SelectedIndexChanged += new System.EventHandler(this.ComboBox_flooderPictureSource_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(14, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Источник картинок";
            // 
            // button_flooderChange
            // 
            this.button_flooderChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_flooderChange.Location = new System.Drawing.Point(221, 369);
            this.button_flooderChange.Name = "button_flooderChange";
            this.button_flooderChange.Size = new System.Drawing.Size(224, 26);
            this.button_flooderChange.TabIndex = 22;
            this.button_flooderChange.Text = "Изменить";
            this.button_flooderChange.UseVisualStyleBackColor = true;
            this.button_flooderChange.Click += new System.EventHandler(this.Button_flooderChange_Click);
            // 
            // button_flooderRemove
            // 
            this.button_flooderRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_flooderRemove.Location = new System.Drawing.Point(451, 369);
            this.button_flooderRemove.Name = "button_flooderRemove";
            this.button_flooderRemove.Size = new System.Drawing.Size(218, 26);
            this.button_flooderRemove.TabIndex = 21;
            this.button_flooderRemove.Text = "Удалить";
            this.button_flooderRemove.UseVisualStyleBackColor = true;
            this.button_flooderRemove.Click += new System.EventHandler(this.Button_flooderRemove_Click);
            // 
            // button_flooderAdd
            // 
            this.button_flooderAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_flooderAdd.Location = new System.Drawing.Point(7, 369);
            this.button_flooderAdd.Name = "button_flooderAdd";
            this.button_flooderAdd.Size = new System.Drawing.Size(208, 26);
            this.button_flooderAdd.TabIndex = 20;
            this.button_flooderAdd.Text = "Добавить";
            this.button_flooderAdd.UseVisualStyleBackColor = true;
            this.button_flooderAdd.Click += new System.EventHandler(this.Button_flooderAdd_Click);
            // 
            // dataGridView_flooderEditable
            // 
            this.dataGridView_flooderEditable.AllowUserToAddRows = false;
            this.dataGridView_flooderEditable.AllowUserToDeleteRows = false;
            this.dataGridView_flooderEditable.AllowUserToResizeRows = false;
            this.dataGridView_flooderEditable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_flooderEditable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2});
            this.dataGridView_flooderEditable.Location = new System.Drawing.Point(7, 303);
            this.dataGridView_flooderEditable.MultiSelect = false;
            this.dataGridView_flooderEditable.Name = "dataGridView_flooderEditable";
            this.dataGridView_flooderEditable.RowHeadersVisible = false;
            this.dataGridView_flooderEditable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_flooderEditable.Size = new System.Drawing.Size(662, 60);
            this.dataGridView_flooderEditable.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Ссылка на цель";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 184;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "Расположение обращения";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Начало",
            "Конец",
            "Вместо ...",
            "В начале и в конце (через :)"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 165;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Обращение";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 155;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "Содержимое";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "Текст",
            "Изображение",
            "Изображение+текст",
            "Голосовое сообщение",
            "Стикер",
            "Текст+стикер",
            "Видеозапись",
            "Видеозапись+текст",
            "Аудиозапись+текст",
            "Аудиозапись+текст+картинка",
            "Аудиозапись+картинка",
            "Документ+текст",
            "damn.ru"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Width = 155;
            // 
            // dataGridView_flooderView
            // 
            this.dataGridView_flooderView.AllowUserToAddRows = false;
            this.dataGridView_flooderView.AllowUserToDeleteRows = false;
            this.dataGridView_flooderView.AllowUserToResizeRows = false;
            this.dataGridView_flooderView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_flooderView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView_flooderView.Location = new System.Drawing.Point(7, 46);
            this.dataGridView_flooderView.MultiSelect = false;
            this.dataGridView_flooderView.Name = "dataGridView_flooderView";
            this.dataGridView_flooderView.ReadOnly = true;
            this.dataGridView_flooderView.RowHeadersVisible = false;
            this.dataGridView_flooderView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView_flooderView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_flooderView.Size = new System.Drawing.Size(662, 251);
            this.dataGridView_flooderView.TabIndex = 0;
            this.dataGridView_flooderView.SelectionChanged += new System.EventHandler(this.DataGridView_flooderView_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Ссылка на цель";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 165;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Расположение обращения";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 165;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Обращение";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 155;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Содержимое";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 155;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.dataGridView_autoansEditable);
            this.tabPage2.Controls.Add(this.dataGridView_autoansView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(686, 402);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Автоответчик";
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(221, 369);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(224, 26);
            this.button5.TabIndex = 22;
            this.button5.Text = "Изменить";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(451, 369);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(218, 26);
            this.button6.TabIndex = 21;
            this.button6.Text = "Удалить";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(7, 369);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(208, 26);
            this.button7.TabIndex = 20;
            this.button7.Text = "Добавить";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // dataGridView_autoansEditable
            // 
            this.dataGridView_autoansEditable.AllowUserToAddRows = false;
            this.dataGridView_autoansEditable.AllowUserToDeleteRows = false;
            this.dataGridView_autoansEditable.AllowUserToResizeRows = false;
            this.dataGridView_autoansEditable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_autoansEditable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn4});
            this.dataGridView_autoansEditable.Location = new System.Drawing.Point(7, 303);
            this.dataGridView_autoansEditable.MultiSelect = false;
            this.dataGridView_autoansEditable.Name = "dataGridView_autoansEditable";
            this.dataGridView_autoansEditable.RowHeadersVisible = false;
            this.dataGridView_autoansEditable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_autoansEditable.Size = new System.Drawing.Size(662, 60);
            this.dataGridView_autoansEditable.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Ссылка на цель";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 165;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.HeaderText = "Расположение обращения";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "Начало",
            "Конец",
            "Вместо ...",
            "В начале и в конце (через :)"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Width = 164;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Обращение";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 165;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.HeaderText = "Содержимое";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "Текст",
            "Текст-ссылка",
            "Изображение",
            "Изображение+текст",
            "Голосовые сообщения",
            "Стикеры",
            "Видеозаписи",
            "Аудиозапись+текст",
            "Аудиозапись+текст+картинка",
            "Аудиозапись+картинка"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Width = 165;
            // 
            // dataGridView_autoansView
            // 
            this.dataGridView_autoansView.AllowUserToAddRows = false;
            this.dataGridView_autoansView.AllowUserToDeleteRows = false;
            this.dataGridView_autoansView.AllowUserToResizeRows = false;
            this.dataGridView_autoansView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_autoansView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn6});
            this.dataGridView_autoansView.Location = new System.Drawing.Point(7, 6);
            this.dataGridView_autoansView.MultiSelect = false;
            this.dataGridView_autoansView.Name = "dataGridView_autoansView";
            this.dataGridView_autoansView.ReadOnly = true;
            this.dataGridView_autoansView.RowHeadersVisible = false;
            this.dataGridView_autoansView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_autoansView.Size = new System.Drawing.Size(662, 291);
            this.dataGridView_autoansView.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Ссылка на цель";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 165;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.HeaderText = "Расположение обращения";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "Начало",
            "Конец",
            "Вместо ...",
            "В начале и в конце (через :)"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.ReadOnly = true;
            this.dataGridViewComboBoxColumn5.Width = 164;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Обращение";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 165;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.HeaderText = "Содержимое";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "Текст",
            "Текст-ссылка",
            "Изображение",
            "Изображение+текст",
            "Голосовые сообщения",
            "Стикеры",
            "Видеозаписи",
            "Аудиозапись+текст",
            "Аудиозапись+текст+картинка",
            "Аудиозапись+картинка"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.ReadOnly = true;
            this.dataGridViewComboBoxColumn6.Width = 165;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.Controls.Add(this.button_chatsChange);
            this.tabPage3.Controls.Add(this.button_chatsRemove);
            this.tabPage3.Controls.Add(this.button_chatsAdd);
            this.tabPage3.Controls.Add(this.dataGridView_chatsEditable);
            this.tabPage3.Controls.Add(this.dataGridView_chatsView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(686, 400);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Беседы";
            // 
            // button_chatsChange
            // 
            this.button_chatsChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_chatsChange.Location = new System.Drawing.Point(221, 369);
            this.button_chatsChange.Name = "button_chatsChange";
            this.button_chatsChange.Size = new System.Drawing.Size(224, 26);
            this.button_chatsChange.TabIndex = 22;
            this.button_chatsChange.Text = "Изменить";
            this.button_chatsChange.UseVisualStyleBackColor = true;
            this.button_chatsChange.Click += new System.EventHandler(this.Button_chatsChange_Click);
            // 
            // button_chatsRemove
            // 
            this.button_chatsRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_chatsRemove.Location = new System.Drawing.Point(451, 369);
            this.button_chatsRemove.Name = "button_chatsRemove";
            this.button_chatsRemove.Size = new System.Drawing.Size(218, 26);
            this.button_chatsRemove.TabIndex = 21;
            this.button_chatsRemove.Text = "Удалить";
            this.button_chatsRemove.UseVisualStyleBackColor = true;
            this.button_chatsRemove.Click += new System.EventHandler(this.Button_chatsRemove_Click);
            // 
            // button_chatsAdd
            // 
            this.button_chatsAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_chatsAdd.Location = new System.Drawing.Point(7, 369);
            this.button_chatsAdd.Name = "button_chatsAdd";
            this.button_chatsAdd.Size = new System.Drawing.Size(208, 26);
            this.button_chatsAdd.TabIndex = 20;
            this.button_chatsAdd.Text = "Добавить";
            this.button_chatsAdd.UseVisualStyleBackColor = true;
            this.button_chatsAdd.Click += new System.EventHandler(this.Button_chatsAdd_Click);
            // 
            // dataGridView_chatsEditable
            // 
            this.dataGridView_chatsEditable.AllowUserToAddRows = false;
            this.dataGridView_chatsEditable.AllowUserToDeleteRows = false;
            this.dataGridView_chatsEditable.AllowUserToResizeRows = false;
            this.dataGridView_chatsEditable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_chatsEditable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn8,
            this.dataGridViewCheckBoxColumn2});
            this.dataGridView_chatsEditable.Location = new System.Drawing.Point(7, 285);
            this.dataGridView_chatsEditable.MultiSelect = false;
            this.dataGridView_chatsEditable.Name = "dataGridView_chatsEditable";
            this.dataGridView_chatsEditable.RowHeadersVisible = false;
            this.dataGridView_chatsEditable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_chatsEditable.Size = new System.Drawing.Size(662, 78);
            this.dataGridView_chatsEditable.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Ссылка на беседу";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 132;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.HeaderText = "Работа с названием";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "Ничего не делать",
            "Заменять на ->",
            "Флудить названиями"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Width = 132;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Название";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 132;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.HeaderText = "Аватар";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "Ничего не делать",
            "Удалять"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Width = 132;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Флудить сменой аватара";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 131;
            // 
            // dataGridView_chatsView
            // 
            this.dataGridView_chatsView.AllowUserToAddRows = false;
            this.dataGridView_chatsView.AllowUserToDeleteRows = false;
            this.dataGridView_chatsView.AllowUserToResizeRows = false;
            this.dataGridView_chatsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_chatsView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn10,
            this.dataGridViewCheckBoxColumn1});
            this.dataGridView_chatsView.Location = new System.Drawing.Point(7, 6);
            this.dataGridView_chatsView.MultiSelect = false;
            this.dataGridView_chatsView.Name = "dataGridView_chatsView";
            this.dataGridView_chatsView.ReadOnly = true;
            this.dataGridView_chatsView.RowHeadersVisible = false;
            this.dataGridView_chatsView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_chatsView.Size = new System.Drawing.Size(662, 273);
            this.dataGridView_chatsView.TabIndex = 10;
            this.dataGridView_chatsView.SelectionChanged += new System.EventHandler(this.DataGridView_chatsView_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Ссылка на беседу";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 132;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.HeaderText = "Работа с названием";
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.ReadOnly = true;
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewComboBoxColumn9.Width = 132;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Название";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 132;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.HeaderText = "Аватар";
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.ReadOnly = true;
            this.dataGridViewComboBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewComboBoxColumn10.Width = 132;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Флудить сменой аватара";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Width = 131;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(686, 402);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Умный автоответчик";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.linkLabel1);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.textBox_yandexKeyVO);
            this.tabPage5.Controls.Add(this.button_sendVO);
            this.tabPage5.Controls.Add(this.numericUpDown_speedVO);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.comboBox_emotionVO);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.comboBox_voiceVO);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.textBox_targetVO);
            this.tabPage5.Controls.Add(this.richTextBox_messageVO);
            this.tabPage5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(686, 402);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Голосовые сообщения";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 384);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(208, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Не более 1000 преобразований в сутки";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(450, 384);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(237, 13);
            this.linkLabel1.TabIndex = 24;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Использует сервис \"Yandex SpeechKit Cloud\"";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(7, 258);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(140, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Yandex speech kit api ключ";
            // 
            // textBox_yandexKeyVO
            // 
            this.textBox_yandexKeyVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox_yandexKeyVO.Location = new System.Drawing.Point(7, 274);
            this.textBox_yandexKeyVO.Name = "textBox_yandexKeyVO";
            this.textBox_yandexKeyVO.Size = new System.Drawing.Size(662, 20);
            this.textBox_yandexKeyVO.TabIndex = 21;
            this.textBox_yandexKeyVO.TextChanged += new System.EventHandler(this.TextBox_yandexKeyVO_TextChanged);
            // 
            // button_sendVO
            // 
            this.button_sendVO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_sendVO.Location = new System.Drawing.Point(459, 228);
            this.button_sendVO.Name = "button_sendVO";
            this.button_sendVO.Size = new System.Drawing.Size(210, 26);
            this.button_sendVO.TabIndex = 20;
            this.button_sendVO.Text = "Отправить";
            this.button_sendVO.UseVisualStyleBackColor = true;
            this.button_sendVO.Click += new System.EventHandler(this.Button_sendVO_Click);
            // 
            // numericUpDown_speedVO
            // 
            this.numericUpDown_speedVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numericUpDown_speedVO.Location = new System.Drawing.Point(311, 234);
            this.numericUpDown_speedVO.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDown_speedVO.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_speedVO.Name = "numericUpDown_speedVO";
            this.numericUpDown_speedVO.Size = new System.Drawing.Size(142, 20);
            this.numericUpDown_speedVO.TabIndex = 19;
            this.numericUpDown_speedVO.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_speedVO.ValueChanged += new System.EventHandler(this.NumericUpDown_speedVO_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(308, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Скорость речи 1..30";
            // 
            // comboBox_emotionVO
            // 
            this.comboBox_emotionVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_emotionVO.FormattingEnabled = true;
            this.comboBox_emotionVO.Items.AddRange(new object[] {
            "neutral",
            "good",
            "evil"});
            this.comboBox_emotionVO.Location = new System.Drawing.Point(166, 234);
            this.comboBox_emotionVO.Name = "comboBox_emotionVO";
            this.comboBox_emotionVO.Size = new System.Drawing.Size(139, 21);
            this.comboBox_emotionVO.TabIndex = 17;
            this.comboBox_emotionVO.SelectedIndexChanged += new System.EventHandler(this.ComboBox_emotionVO_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(166, 218);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Эмоция";
            // 
            // comboBox_voiceVO
            // 
            this.comboBox_voiceVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_voiceVO.FormattingEnabled = true;
            this.comboBox_voiceVO.Items.AddRange(new object[] {
            "zahar",
            "ermil",
            "jane",
            "oksana",
            "alyss",
            "omazh"});
            this.comboBox_voiceVO.Location = new System.Drawing.Point(7, 234);
            this.comboBox_voiceVO.Name = "comboBox_voiceVO";
            this.comboBox_voiceVO.Size = new System.Drawing.Size(153, 21);
            this.comboBox_voiceVO.TabIndex = 15;
            this.comboBox_voiceVO.SelectedIndexChanged += new System.EventHandler(this.ComboBox_voiceVO_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(7, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Голос";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(7, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Цель (лс/чат)";
            // 
            // textBox_targetVO
            // 
            this.textBox_targetVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox_targetVO.Location = new System.Drawing.Point(7, 195);
            this.textBox_targetVO.Name = "textBox_targetVO";
            this.textBox_targetVO.Size = new System.Drawing.Size(662, 20);
            this.textBox_targetVO.TabIndex = 12;
            // 
            // richTextBox_messageVO
            // 
            this.richTextBox_messageVO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.richTextBox_messageVO.Location = new System.Drawing.Point(7, 8);
            this.richTextBox_messageVO.Name = "richTextBox_messageVO";
            this.richTextBox_messageVO.Size = new System.Drawing.Size(662, 170);
            this.richTextBox_messageVO.TabIndex = 11;
            this.richTextBox_messageVO.Text = "";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(686, 402);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Инвайтер";
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tabPage8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage8.Controls.Add(this.label22);
            this.tabPage8.Controls.Add(this.richTextBox1);
            this.tabPage8.Controls.Add(this.label21);
            this.tabPage8.Controls.Add(this.textBox5);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(686, 402);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Удалённое управление";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(269, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(120, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Справка по командам";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Location = new System.Drawing.Point(7, 63);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(662, 334);
            this.richTextBox1.TabIndex = 24;
            this.richTextBox1.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(245, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(177, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Ссылка на управляющий аккаунт";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox5.Location = new System.Drawing.Point(7, 24);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(662, 20);
            this.textBox5.TabIndex = 22;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_log,
            this.toolStripMenuItem_manager,
            this.toolStripMenuItem_help,
            this.toolStripMenuItem_reload});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(695, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip";
            // 
            // toolStripMenuItem_log
            // 
            this.toolStripMenuItem_log.Name = "toolStripMenuItem_log";
            this.toolStripMenuItem_log.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItem_log.Text = "Лог";
            this.toolStripMenuItem_log.Click += new System.EventHandler(this.ToolStripMenuItem_log_Click);
            // 
            // toolStripMenuItem_manager
            // 
            this.toolStripMenuItem_manager.Name = "toolStripMenuItem_manager";
            this.toolStripMenuItem_manager.Size = new System.Drawing.Size(96, 20);
            this.toolStripMenuItem_manager.Text = "Менеджер .txt";
            this.toolStripMenuItem_manager.Click += new System.EventHandler(this.ToolStripMenuItem_manager_Click);
            // 
            // toolStripMenuItem_help
            // 
            this.toolStripMenuItem_help.Name = "toolStripMenuItem_help";
            this.toolStripMenuItem_help.Size = new System.Drawing.Size(65, 20);
            this.toolStripMenuItem_help.Text = "Справка";
            this.toolStripMenuItem_help.Click += new System.EventHandler(this.ToolStripMenuItem_help_Click);
            // 
            // toolStripMenuItem_reload
            // 
            this.toolStripMenuItem_reload.Name = "toolStripMenuItem_reload";
            this.toolStripMenuItem_reload.Size = new System.Drawing.Size(145, 20);
            this.toolStripMenuItem_reload.Text = "Перезагрузить контент";
            this.toolStripMenuItem_reload.Click += new System.EventHandler(this.ToolStripMenuItem_reload_Click);
            // 
            // dateTimePicker_timeToStop
            // 
            this.dateTimePicker_timeToStop.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.dateTimePicker_timeToStop.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.dateTimePicker_timeToStop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_timeToStop.Location = new System.Drawing.Point(483, 1);
            this.dateTimePicker_timeToStop.Name = "dateTimePicker_timeToStop";
            this.dateTimePicker_timeToStop.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_timeToStop.TabIndex = 2;
            // 
            // checkBox_shutownOnTime
            // 
            this.checkBox_shutownOnTime.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_shutownOnTime.Location = new System.Drawing.Point(357, 2);
            this.checkBox_shutownOnTime.Name = "checkBox_shutownOnTime";
            this.checkBox_shutownOnTime.OffFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_shutownOnTime.OnFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_shutownOnTime.Size = new System.Drawing.Size(45, 19);
            this.checkBox_shutownOnTime.Style = JCS.ToggleSwitch.ToggleSwitchStyle.Modern;
            this.checkBox_shutownOnTime.TabIndex = 3;
            // 
            // comboBox_accountsList
            // 
            this.comboBox_accountsList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_accountsList.FormattingEnabled = true;
            this.comboBox_accountsList.Location = new System.Drawing.Point(123, 542);
            this.comboBox_accountsList.Name = "comboBox_accountsList";
            this.comboBox_accountsList.Size = new System.Drawing.Size(559, 21);
            this.comboBox_accountsList.TabIndex = 17;
            this.comboBox_accountsList.Text = "Загрузка...";
            this.comboBox_accountsList.SelectedIndexChanged += new System.EventHandler(this.ComboBox_accountsList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(5, 546);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Выбранный аккаунт:";
            // 
            // textBox_manualCaptchaAnswer
            // 
            this.textBox_manualCaptchaAnswer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox_manualCaptchaAnswer.Location = new System.Drawing.Point(560, 86);
            this.textBox_manualCaptchaAnswer.Name = "textBox_manualCaptchaAnswer";
            this.textBox_manualCaptchaAnswer.Size = new System.Drawing.Size(130, 20);
            this.textBox_manualCaptchaAnswer.TabIndex = 12;
            this.textBox_manualCaptchaAnswer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_manualCaptchaAnswer_KeyDown);
            // 
            // radioButton_captchaManual
            // 
            this.radioButton_captchaManual.AutoSize = true;
            this.radioButton_captchaManual.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_captchaManual.Location = new System.Drawing.Point(451, 50);
            this.radioButton_captchaManual.Name = "radioButton_captchaManual";
            this.radioButton_captchaManual.Size = new System.Drawing.Size(66, 17);
            this.radioButton_captchaManual.TabIndex = 13;
            this.radioButton_captchaManual.Text = "вручную";
            this.radioButton_captchaManual.UseVisualStyleBackColor = false;
            this.radioButton_captchaManual.CheckedChanged += new System.EventHandler(this.RadioButton_captchaManual_CheckedChanged);
            // 
            // radioButton_captchaRucap
            // 
            this.radioButton_captchaRucap.AutoSize = true;
            this.radioButton_captchaRucap.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_captchaRucap.Location = new System.Drawing.Point(451, 69);
            this.radioButton_captchaRucap.Name = "radioButton_captchaRucap";
            this.radioButton_captchaRucap.Size = new System.Drawing.Size(96, 17);
            this.radioButton_captchaRucap.TabIndex = 14;
            this.radioButton_captchaRucap.Text = "rucaptcha.com";
            this.radioButton_captchaRucap.UseVisualStyleBackColor = false;
            this.radioButton_captchaRucap.CheckedChanged += new System.EventHandler(this.RadioButton_captchaRucap_CheckedChanged);
            // 
            // radioButton_captchaAnticap
            // 
            this.radioButton_captchaAnticap.AutoSize = true;
            this.radioButton_captchaAnticap.BackColor = System.Drawing.Color.Transparent;
            this.radioButton_captchaAnticap.Location = new System.Drawing.Point(451, 87);
            this.radioButton_captchaAnticap.Name = "radioButton_captchaAnticap";
            this.radioButton_captchaAnticap.Size = new System.Drawing.Size(107, 17);
            this.radioButton_captchaAnticap.TabIndex = 15;
            this.radioButton_captchaAnticap.Text = "anti-captcha.com";
            this.radioButton_captchaAnticap.UseVisualStyleBackColor = false;
            this.radioButton_captchaAnticap.CheckedChanged += new System.EventHandler(this.RadioButton_captchaAnticap_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(448, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Антикапча";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(25, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 33;
            this.label19.Text = "Старт/стоп";
            // 
            // label_forAll
            // 
            this.label_forAll.AutoSize = true;
            this.label_forAll.BackColor = System.Drawing.Color.Transparent;
            this.label_forAll.Location = new System.Drawing.Point(91, 95);
            this.label_forAll.Name = "label_forAll";
            this.label_forAll.Size = new System.Drawing.Size(78, 13);
            this.label_forAll.TabIndex = 34;
            this.label_forAll.Text = "Для всех: вкл";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(180, 95);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Антикапча";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(261, 95);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 36;
            this.label25.Text = "Дизайн";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(330, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "Доп. боты";
            // 
            // timer_manualCaptcha
            // 
            this.timer_manualCaptcha.Enabled = true;
            this.timer_manualCaptcha.Interval = 1000;
            this.timer_manualCaptcha.Tick += new System.EventHandler(this.Timer_manualCaptcha_Tick);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.Text = "notifyIcon";
            this.notifyIcon.Visible = true;
            // 
            // timer_shutown
            // 
            this.timer_shutown.Enabled = true;
            this.timer_shutown.Interval = 1000;
            this.timer_shutown.Tick += new System.EventHandler(this.Timer_shutown_Tick);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Location = new System.Drawing.Point(403, 5);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(76, 13);
            this.label37.TabIndex = 40;
            this.label37.Text = "Остановить в";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(94, 20);
            // 
            // button_otherBots
            // 
            this.button_otherBots.BackColor = System.Drawing.Color.Transparent;
            this.button_otherBots.BackgroundImage = global::ISP.Properties.Resources.OtherBots;
            this.button_otherBots.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_otherBots.FlatAppearance.BorderSize = 0;
            this.button_otherBots.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_otherBots.Location = new System.Drawing.Point(328, 28);
            this.button_otherBots.Name = "button_otherBots";
            this.button_otherBots.Size = new System.Drawing.Size(65, 65);
            this.button_otherBots.TabIndex = 26;
            this.button_otherBots.UseVisualStyleBackColor = false;
            this.button_otherBots.Click += new System.EventHandler(this.Button_otherBots_Click);
            this.button_otherBots.MouseEnter += new System.EventHandler(this.Button_interfaceMouseEnter);
            this.button_otherBots.MouseLeave += new System.EventHandler(this.Button_interfaceMouseLeave);
            // 
            // button_design
            // 
            this.button_design.BackColor = System.Drawing.Color.Transparent;
            this.button_design.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_design.BackgroundImage")));
            this.button_design.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_design.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_design.FlatAppearance.BorderSize = 0;
            this.button_design.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_design.Location = new System.Drawing.Point(252, 28);
            this.button_design.Name = "button_design";
            this.button_design.Size = new System.Drawing.Size(65, 65);
            this.button_design.TabIndex = 21;
            this.button_design.UseVisualStyleBackColor = false;
            this.button_design.Click += new System.EventHandler(this.Button_anticaptchaDesign_Click);
            this.button_design.MouseEnter += new System.EventHandler(this.Button_interfaceMouseEnter);
            this.button_design.MouseLeave += new System.EventHandler(this.Button_interfaceMouseLeave);
            // 
            // button_anticaptchaKeys
            // 
            this.button_anticaptchaKeys.BackColor = System.Drawing.Color.Transparent;
            this.button_anticaptchaKeys.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_anticaptchaKeys.BackgroundImage")));
            this.button_anticaptchaKeys.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_anticaptchaKeys.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_anticaptchaKeys.FlatAppearance.BorderSize = 0;
            this.button_anticaptchaKeys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_anticaptchaKeys.Location = new System.Drawing.Point(176, 28);
            this.button_anticaptchaKeys.Name = "button_anticaptchaKeys";
            this.button_anticaptchaKeys.Size = new System.Drawing.Size(65, 65);
            this.button_anticaptchaKeys.TabIndex = 20;
            this.button_anticaptchaKeys.UseVisualStyleBackColor = false;
            this.button_anticaptchaKeys.Click += new System.EventHandler(this.Button_anticaptchaKeys_Click);
            this.button_anticaptchaKeys.MouseEnter += new System.EventHandler(this.Button_interfaceMouseEnter);
            this.button_anticaptchaKeys.MouseLeave += new System.EventHandler(this.Button_interfaceMouseLeave);
            // 
            // button_applyToAll
            // 
            this.button_applyToAll.BackColor = System.Drawing.Color.Transparent;
            this.button_applyToAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_applyToAll.BackgroundImage")));
            this.button_applyToAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_applyToAll.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_applyToAll.FlatAppearance.BorderSize = 0;
            this.button_applyToAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_applyToAll.Location = new System.Drawing.Point(100, 28);
            this.button_applyToAll.Name = "button_applyToAll";
            this.button_applyToAll.Size = new System.Drawing.Size(65, 65);
            this.button_applyToAll.TabIndex = 19;
            this.button_applyToAll.UseVisualStyleBackColor = false;
            this.button_applyToAll.Click += new System.EventHandler(this.Button_applyToAll_Click);
            this.button_applyToAll.MouseEnter += new System.EventHandler(this.Button_interfaceMouseEnter);
            this.button_applyToAll.MouseLeave += new System.EventHandler(this.Button_interfaceMouseLeave);
            // 
            // button_startStop
            // 
            this.button_startStop.BackColor = System.Drawing.Color.Transparent;
            this.button_startStop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_startStop.BackgroundImage")));
            this.button_startStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_startStop.Enabled = false;
            this.button_startStop.FlatAppearance.BorderSize = 0;
            this.button_startStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_startStop.Location = new System.Drawing.Point(24, 28);
            this.button_startStop.Name = "button_startStop";
            this.button_startStop.Size = new System.Drawing.Size(65, 65);
            this.button_startStop.TabIndex = 0;
            this.button_startStop.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.button_startStop.UseVisualStyleBackColor = false;
            this.button_startStop.Click += new System.EventHandler(this.Button_startStop_Click);
            this.button_startStop.MouseEnter += new System.EventHandler(this.Button_interfaceMouseEnter);
            this.button_startStop.MouseLeave += new System.EventHandler(this.Button_interfaceMouseLeave);
            // 
            // pictureBox_captPic
            // 
            this.pictureBox_captPic.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox_captPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_captPic.Location = new System.Drawing.Point(560, 33);
            this.pictureBox_captPic.Name = "pictureBox_captPic";
            this.pictureBox_captPic.Size = new System.Drawing.Size(130, 50);
            this.pictureBox_captPic.TabIndex = 11;
            this.pictureBox_captPic.TabStop = false;
            // 
            // contextMenuStrip_otherBots
            // 
            this.contextMenuStrip_otherBots.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_typerBot,
            this.toolStripMenuItem_antikickBot});
            this.contextMenuStrip_otherBots.Name = "contextMenuStrip_otherBots";
            this.contextMenuStrip_otherBots.Size = new System.Drawing.Size(148, 48);
            // 
            // toolStripMenuItem_typerBot
            // 
            this.toolStripMenuItem_typerBot.Name = "toolStripMenuItem_typerBot";
            this.toolStripMenuItem_typerBot.Size = new System.Drawing.Size(147, 22);
            this.toolStripMenuItem_typerBot.Text = "Набор текста";
            this.toolStripMenuItem_typerBot.Click += new System.EventHandler(this.ToolStripMenuItem_typerBot_Click);
            // 
            // toolStripMenuItem_antikickBot
            // 
            this.toolStripMenuItem_antikickBot.Enabled = false;
            this.toolStripMenuItem_antikickBot.Name = "toolStripMenuItem_antikickBot";
            this.toolStripMenuItem_antikickBot.Size = new System.Drawing.Size(147, 22);
            this.toolStripMenuItem_antikickBot.Text = "Антикик";
            this.toolStripMenuItem_antikickBot.Click += new System.EventHandler(this.ToolStripMenuItem_antikickBot_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(695, 567);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label_forAll);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.button_otherBots);
            this.Controls.Add(this.button_design);
            this.Controls.Add(this.button_anticaptchaKeys);
            this.Controls.Add(this.button_applyToAll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox_accountsList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_startStop);
            this.Controls.Add(this.radioButton_captchaAnticap);
            this.Controls.Add(this.radioButton_captchaRucap);
            this.Controls.Add(this.radioButton_captchaManual);
            this.Controls.Add(this.textBox_manualCaptchaAnswer);
            this.Controls.Add(this.pictureBox_captPic);
            this.Controls.Add(this.checkBox_shutownOnTime);
            this.Controls.Add(this.dateTimePicker_timeToStop);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "ISB";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.tabControl.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_remoteDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_inviterDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_smartautoDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_chatsDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_autoansDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_flooderDelay)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_flooderEditable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_flooderView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_autoansEditable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_autoansView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_chatsEditable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_chatsView)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_speedVO)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_captPic)).EndInit();
            this.contextMenuStrip_otherBots.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private Timer timer_manualCaptcha;
        private Button button_flooderChange;
        private Button button_flooderRemove;
        private Button button_flooderAdd;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button_chatsChange;
        private Button button_chatsRemove;
        private Button button_chatsAdd;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private NotifyIcon notifyIcon;
        private Label label3;
        private ComboBox comboBox_flooderPhrasesWithDotsSource;
        private Label label27;
        private ComboBox comboBox_flooderPhrasesSource;
        private Label label4;
        private ComboBox comboBox_flooderPictureSource;
        private Timer timer_shutown;
        private ComboBox comboBox_flooderStickerFile;
        private Label label28;
        private ComboBox comboBox_flooderStickerId;
        private Label label29;
        private Label label36;
        private Label label35;
        private Label label33;
        private Label label32;
        private Label label31;
        private Label label30;
        private Label label37;
        private ToolStripMenuItem toolStripMenuItem_manager;
        private ToolStripMenuItem toolStripMenuItem1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private ToolStripMenuItem toolStripMenuItem_reload;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewComboBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewComboBoxColumn10;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private ContextMenuStrip contextMenuStrip_otherBots;
        private ToolStripMenuItem toolStripMenuItem_typerBot;
        private ToolStripMenuItem toolStripMenuItem_antikickBot;
        private TabPage tabPage5;
        private LinkLabel linkLabel1;
        private Label label16;
        private TextBox textBox_yandexKeyVO;
        private Button button_sendVO;
        private NumericUpDown numericUpDown_speedVO;
        private Label label8;
        private ComboBox comboBox_emotionVO;
        private Label label14;
        private ComboBox comboBox_voiceVO;
        private Label label10;
        private Label label12;
        private TextBox textBox_targetVO;
        private RichTextBox richTextBox_messageVO;
        private Label label13;
    }
}
