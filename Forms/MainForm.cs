﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ISP.Captcha;
using ISP.Configs;
using ISP.Engine.Accounts;
using ISP.Forms.Dialogs;
using ISP.Misc;
using ISP.Properties;
using ISP.Tasks.Settings;
using JCS;

namespace ISP.Forms
{
    internal sealed partial class MainForm : Form
    {
        private string _currManualCaptId;

        private AnticapSettingsForm _formAnticapSettingsForm;
        private DesignForm _formDesignForm;
        private HelpForm _formHelpForm;
        private LogForm _formLogForm;
        private TyperForm _formTyperForm;
        private TxtManagerForm _formTxtManagerForm;
        private AntikickForm _formAntikickForm;

        private bool _accountsWorking;

        private static NotifyIcon _ni;

        private bool _notUserTriggeredChange = false;

        public MainForm()
        {
            InitializeComponent();

            linkLabel1.Links.Add(new LinkLabel.Link() { LinkData = "https://tech.yandex.ru/speechkit/cloud/" });

            DesignForm.FormMainForm = this;
            notifyIcon.Icon = Icon;
            _ni = notifyIcon;

            _accountsWorking = false;

            Directory.CreateDirectory("Txts");
            Directory.CreateDirectory("Txts\\Attachments");
            Directory.CreateDirectory("Txts\\Phrases");
            Directory.CreateDirectory("Txts\\Stickers");
            Directory.CreateDirectory("Txts\\Phrases\\Simple");
            Directory.CreateDirectory("Txts\\Phrases\\Dots");
            Directory.CreateDirectory("Accounts");
            Directory.CreateDirectory("Configs");
            Directory.CreateDirectory("Tmp");
            Directory.CreateDirectory("Upload");
            Directory.CreateDirectory("Upload\\Documents");
            Directory.CreateDirectory("Upload\\Images");
            Directory.CreateDirectory("Upload\\Avatars");
            foreach (string tmp in Directory.GetFiles("Tmp"))
                File.Delete(tmp);
            CreateIfNotExists("Txts\\accounts.txt");
            CreateIfNotExists("Txts\\typerTexts.txt");
            CreateIfNotExists("Txts\\titles.txt");
            CreateIfNotExists("Txts\\Attachments\\audios.txt");
            CreateIfNotExists("Txts\\Attachments\\images.txt");
            CreateIfNotExists("Txts\\Attachments\\videos.txt");

            ConfigController.Load();

            try
            {
                SetTabsBackPicture(ConfigController.InterfaceConfig.PathToBackgroundImgTabs);
            }
            catch
            {
            }
            try
            {
                SetBackPicture(ConfigController.InterfaceConfig.PathToBackgroundImgGeneral);
            }
            catch
            {
            }
            try
            {
                SetForeColor(Color.FromArgb(ConfigController.InterfaceConfig.ForColorAsArgb));
            }
            catch
            {
            }
            switch (ConfigController.AnticapConfig.SelectedMode)
            {
                case SelectedMode.Anticaptcha:
                    radioButton_captchaAnticap.Checked = true;
                    break;
                case SelectedMode.Rucaptcha:
                    radioButton_captchaRucap.Checked = true;
                    break;
                case SelectedMode.Manual:
                    radioButton_captchaManual.Checked = true;
                    break;
            }

            ReloadFileLists();
        }

        private void ReloadFileLists()
        {
            var txtss = new List<string>(Directory.GetFiles("Txts\\Phrases\\Simple"));
            txtss = txtss.ConvertAll(Path.GetFileName);
            var txtsd = new List<string>(Directory.GetFiles("Txts\\Phrases\\Dots"));
            txtsd = txtsd.ConvertAll(Path.GetFileName);
            var stks = new List<string>(Directory.GetFiles("Txts\\Stickers"));
            stks = stks.ConvertAll(Path.GetFileName);
            var avs = new List<string>(Directory.GetFiles("Upload\\Avatars"));
            avs = avs.ConvertAll(Path.GetFileName);
            comboBox_flooderPhrasesSource.Items.Clear();
            comboBox_flooderPhrasesSource.Items.AddRange(txtss.ToArray());
            comboBox_flooderPhrasesWithDotsSource.Items.Clear();
            comboBox_flooderPhrasesWithDotsSource.Items.AddRange(txtsd.ToArray());
            comboBox_flooderStickerFile.Items.Clear();
            comboBox_flooderStickerFile.Items.AddRange(stks.ToArray());
            var comboBoxChatsEditable = (DataGridViewComboBoxColumn)dataGridView_chatsEditable.Columns[3];
            comboBoxChatsEditable.Items.Clear();
            comboBoxChatsEditable.Items.Add("Ничего не делать");
            comboBoxChatsEditable.Items.Add("Удалять");
            foreach (string av in avs)
            {
                comboBoxChatsEditable.Items.Add(av);
            }
        }

        public static void NotifyInTray(string msg)
        {
            Console.Beep();
            _ni.BalloonTipText = msg;
            _ni.ShowBalloonTip(5000);
        }

        public void SetTabsBackPicture(string img)
        {
            foreach (Control tabPage in tabControl.TabPages)
                tabPage.BackgroundImage = img == null ? null : Image.FromFile(img);
            ConfigController.InterfaceConfig.PathToBackgroundImgTabs = img;
            ConfigController.InterfaceConfig.Save();
        }

        public void SetBackPicture(string img)
        {
            BackgroundImage = img == null ? null : Image.FromFile(img);
            ConfigController.InterfaceConfig.PathToBackgroundImgGeneral = img;
            ConfigController.InterfaceConfig.Save();
        }

        public void SetForeColor(Color col)
        {
            Color colorForSwitches = col.ToArgb() == Color.Black.ToArgb() ? Color.Gray : col;
            ToggleSwitchModernRenderer switchRenderer = new ToggleSwitchModernRenderer()
            {
                LeftSideBackColor1 = colorForSwitches,
                LeftSideBackColor2 = colorForSwitches,
                ArrowNormalColor = colorForSwitches,
                ArrowHoverColor = Color.White,
                ArrowPressedColor = Color.White
            };

            checkBox_inviterEnabled.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_autoansEnabled.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_chatsEnabled.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_flooderEnabled.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_remoteEnabled.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_shutownOnTime.SetRenderer(Cloner.Clone(switchRenderer));
            checkBox_smartautoEnabled.SetRenderer(Cloner.Clone(switchRenderer));

            ForeColor = col;
            menuStrip.ForeColor = col;
            foreach (Control tabPage in tabControl.TabPages)
                tabPage.ForeColor = col;
            ConfigController.InterfaceConfig.ForColorAsArgb = col.ToArgb();
            ConfigController.InterfaceConfig.Save();
        }

        private string TwoFactHandler()
        {
            return (string)Invoke(new Func<string>(() =>
            {
                using (TwoFactAsker tfa = new TwoFactAsker())
                {
                    if (tfa.ShowDialog(this) == DialogResult.OK)
                    {
                        return tfa.GetCode();
                    }
                    else
                    {
                        return null;
                    }
                }
            }));
        }

        private static void CreateIfNotExists(string name)
        {
            if (!File.Exists(name))
                File.WriteAllText(name, "");
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            MaximizeBox = false;

            if (!File.Exists("AngleSharp.dll") || !File.Exists("JetBrains.Annotations.dll") || !File.Exists("Newtonsoft.Json.dll") || !File.Exists("VkNet.UWP.dll") || !File.Exists("ToggleSwitch.dll"))
            {
                MessageBox.Show("Отсутствуют необходимые для работы dll (AngleSharp.dll, JetBrains.Annotations.dll, Newtonsoft.Json.dll, VkNet.UWP.dll, ToggleSwitch.dll), продолжение выполнения невозможно", "Dll не найдены", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            LogForm.PushToLog("Авторизация аккаунтов...");
            new Task(() =>
            {
                AccountsManager.LoadAndPrepareAccounts(TwoFactHandler);
                comboBox_accountsList.Invoke(new Action(() =>
                {
                    foreach (string acc in AccountsManager.GetAccountsList())
                        comboBox_accountsList.Items.Add(acc);
                    if (comboBox_accountsList.Items.Count != 0)
                    {
                        comboBox_accountsList.SelectedIndex = 0;
                        tabControl.Enabled = true;
                        toolStripMenuItem_antikickBot.Enabled = true;
                        button_startStop.Enabled = true;
                    }
                    else
                        comboBox_accountsList.Text = "Не было загружено ни одного аккаунта";
                    LogForm.PushToLog("Авторизация завершена");
                }));
            }).Start();
        }

        private void ToolStripMenuItem_log_Click(object sender, EventArgs e)
        {
            _formLogForm?.Close();
            _formLogForm = new LogForm();
            _formLogForm.Show(this);
        }

        private void ToolStripMenuItem_help_Click(object sender, EventArgs e)
        {
            _formHelpForm?.Close();
            _formHelpForm = new HelpForm();
            _formHelpForm.Show(this);
        }

        private void ToolStripMenuItem_manager_Click(object sender, EventArgs e)
        {
            _formTxtManagerForm?.Close();
            _formTxtManagerForm = new TxtManagerForm();
            _formTxtManagerForm.Show(this);
        }

        private void Button_anticaptchaKeys_Click(object sender, EventArgs e)
        {
            _formAnticapSettingsForm?.Close();
            _formAnticapSettingsForm = new AnticapSettingsForm();
            _formAnticapSettingsForm.Show(this);
        }

        private void Button_anticaptchaDesign_Click(object sender, EventArgs e)
        {
            _formDesignForm?.Close();
            _formDesignForm = new DesignForm();
            _formDesignForm.Show(this);
        }

        private void Button_otherBots_Click(object sender, EventArgs e)
        {
            contextMenuStrip_otherBots.Show(MousePosition);
        }

        private void Button_interfaceMouseEnter(object sender, EventArgs e)
        {
            ((ButtonBase)sender).UseVisualStyleBackColor = false;
            ((Control)sender).BackColor = Color.Silver;
        }

        private void Button_interfaceMouseLeave(object sender, EventArgs e)
        {
            ((ButtonBase)sender).UseVisualStyleBackColor = true;
            ((Control)sender).BackColor = Color.Transparent;
        }

        private int _changeDisablersRunning = 0;
        private void RunChangeEventDisabler()
        {
            _changeDisablersRunning++;
            new Task(() =>
            {
                _notUserTriggeredChange = true;
                Thread.Sleep(100);
                _changeDisablersRunning--;
                if (_changeDisablersRunning == 0)
                    _notUserTriggeredChange = false;
            }).Start();
        }

        private void ComboBox_accountsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_accountsList.SelectedIndex == -1)
                return;

            AccountsManager.SetActiveAccount(comboBox_accountsList.Items[comboBox_accountsList.SelectedIndex].ToString());

            RunChangeEventDisabler();
            LoadGlobalSettingsFormFromActive();
            LoadFlooderFormFromActive();
            LoadChatsFormFromActive();
            LoadVoiceFormFromActive();
            LoadInviterFormFromActive();
        }

        private void RadioButton_captchaManual_CheckedChanged(object sender, EventArgs e)
        {
            ConfigController.AnticapConfig.SelectedMode = SelectedMode.Manual;
            ConfigController.AnticapConfig.Save();
        }

        private void RadioButton_captchaRucap_CheckedChanged(object sender, EventArgs e)
        {
            ConfigController.AnticapConfig.SelectedMode = SelectedMode.Rucaptcha;
            ConfigController.AnticapConfig.Save();
        }

        private void RadioButton_captchaAnticap_CheckedChanged(object sender, EventArgs e)
        {
            ConfigController.AnticapConfig.SelectedMode = SelectedMode.Anticaptcha;
            ConfigController.AnticapConfig.Save();
        }

        private void Timer_manualCaptcha_Tick(object sender, EventArgs e)
        {
            if (pictureBox_captPic.Image == null && CaptchaSolver.CaptchaManualIdsToSolve.Count != 0)
            {
                _currManualCaptId = CaptchaSolver.CaptchaManualIdsToSolve.Dequeue();
                FileStream fs = new FileStream($"tmp\\{_currManualCaptId}.png", FileMode.Open);
                pictureBox_captPic.Image = Image.FromStream(fs);
                fs.Close();
                File.Delete(_currManualCaptId + ".png");
            }
        }

        private void TextBox_manualCaptchaAnswer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter || textBox_manualCaptchaAnswer.Text == "" || _currManualCaptId == null)
                return;

            CaptchaSolver.SetManualAns(_currManualCaptId, textBox_manualCaptchaAnswer.Text);
            pictureBox_captPic.Image.Dispose();
            pictureBox_captPic.Image = null;
            _currManualCaptId = null;
            textBox_manualCaptchaAnswer.Clear();
        }

        private void Button_startStop_Click(object sender, EventArgs e)
        {
            if (!_accountsWorking)
            {
                LogForm.PushToLog("Бот запущен");
                toolStripMenuItem_reload.Enabled = false;
                button_startStop.BackgroundImage = Resources.Stop;
                AccountsManager.StartAllTasks();
            }
            else
            {
                LogForm.PushToLog("Бот остановлен");
                toolStripMenuItem_reload.Enabled = true;
                button_startStop.BackgroundImage = Resources.Start;
                AccountsManager.StopAllTasks();
            }
            _accountsWorking = !_accountsWorking;
        }

        private void Timer_shutown_Tick(object sender, EventArgs e)
        {
            if (checkBox_shutownOnTime.Checked && dateTimePicker_timeToStop.Value <= DateTime.Now && _accountsWorking)
                Button_startStop_Click(null, null);
        }

        private void Button_applyToAll_Click(object sender, EventArgs e)
        {
            if (AccountsManager.ToggleToAll())
            {
                LogForm.PushToLog("Теперь действия применяются для всех");
                label_forAll.Text = "Для всех: выкл";
            }
            else
            {
                LogForm.PushToLog("Теперь действия применяются только для выбранного аккаунта");
                label_forAll.Text = "Для всех: вкл";
            }
        }

        private void ToolStripMenuItem_reload_Click(object sender, EventArgs e)
        {
            LogForm.PushToLog("Перезагрузка .txt и списков файлов...");
            ChatsTaskSettings.ReloadFromTxts();
            FlooderTaskSettings.ReloadFromTxts();
            ReloadFileLists();
            ComboBox_accountsList_SelectedIndexChanged(null, null);
        }

        private void ToolStripMenuItem_typerBot_Click(object sender, EventArgs e)
        {
            _formTyperForm?.Close();
            _formTyperForm = new TyperForm();
            _formTyperForm.Show(this);
        }

        private void ToolStripMenuItem_antikickBot_Click(object sender, EventArgs e)
        {
            _formAntikickForm?.Close();
            _formAntikickForm = new AntikickForm();
            _formAntikickForm.Show(this);
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData as string);
        }
    }
}
