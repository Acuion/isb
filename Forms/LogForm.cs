﻿using System;
using System.Collections.Generic;
using ISP.Engine.Accounts;

namespace ISP.Forms
{
    internal partial class LogForm : CenteredForm
    {
        private static readonly LinkedList<string> Logs = new LinkedList<string>();

        public static void PushToLog(string info)
        {
            PushToLog(null, info);
        }

        public static void PushToLog(Account acc, string info)
        {
            lock (Logs)
            {
                string from = "";
                if (acc != null)
                    from = " " + acc.Login;
                Logs.AddFirst($"[{DateTime.Now.ToShortTimeString()}{from}]: {info}");
            }
        }

        public LogForm()
        {
            InitializeComponent();
            LogUpdater_Tick(null, null);
        }

        private void LogUpdater_Tick(object sender, EventArgs e)
        {
            lock (Logs)
            {
                richTextBox_log.Clear();
                foreach (string entry in Logs)
                {
                    richTextBox_log.Text += entry + "\n";
                }
                if (Logs.Count > 100)
                    Logs.RemoveLast();
            }
        }
    }
}
