﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ISP.Forms
{
    partial class TyperForm
    {
        private IContainer components = null;
        private RichTextBox richTextBox;
        private Button button_launch;
        private Label label4;
        private NumericUpDown numericUpDown_sendingDelay;
        private Label label3;
        private NumericUpDown numericUpDown_typingDelay;
        private Label label_textsCount;
        private Label label1;
        private TextBox textBox_name;
        private Label label2;
        private ComboBox comboBox_placement;
        private Label label5;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            richTextBox = new RichTextBox();
            button_launch = new Button();
            label4 = new Label();
            numericUpDown_sendingDelay = new NumericUpDown();
            label3 = new Label();
            numericUpDown_typingDelay = new NumericUpDown();
            label_textsCount = new Label();
            label1 = new Label();
            textBox_name = new TextBox();
            label2 = new Label();
            comboBox_placement = new ComboBox();
            label5 = new Label();
            numericUpDown_sendingDelay.BeginInit();
            numericUpDown_typingDelay.BeginInit();
            SuspendLayout();
            richTextBox.BackColor = SystemColors.WindowFrame;
            richTextBox.BorderStyle = BorderStyle.None;
            richTextBox.Location = new Point(254, 11);
            richTextBox.Name = "richTextBox";
            richTextBox.ReadOnly = true;
            richTextBox.Size = new Size(239, 91);
            richTextBox.TabIndex = 15;
            richTextBox.Text = "После нажатия на старт через 5 секунд начнётся ввод текста (за это время необходимо поместить курсор в поле ввода)\n\nОстановка производится нажатием на стоп или Alt+T\n";
            button_launch.FlatStyle = FlatStyle.Flat;
            button_launch.Location = new Point(7, 148);
            button_launch.Name = "button_launch";
            button_launch.Size = new Size(480, 23);
            button_launch.TabIndex = 14;
            button_launch.Text = "Старт (Alt+T)";
            button_launch.UseVisualStyleBackColor = true;
            button_launch.Click += new EventHandler(Button_launch_Click);
            label4.AutoSize = true;
            label4.Location = new Point(4, 69);
            label4.Name = "label4";
            label4.Size = new Size(244, 13);
            label4.TabIndex = 13;
            label4.Text = "Задержка между отправками сообщений (мс):";
            numericUpDown_sendingDelay.BackColor = Color.WhiteSmoke;
            numericUpDown_sendingDelay.Location = new Point(7, 85);
            numericUpDown_sendingDelay.Maximum = new Decimal(new int[4]
            {
        1000000000,
        0,
        0,
        0
            });
            numericUpDown_sendingDelay.Minimum = new Decimal(new int[4]
            {
        1,
        0,
        0,
        0
            });
            numericUpDown_sendingDelay.Name = "numericUpDown_sendingDelay";
            numericUpDown_sendingDelay.Size = new Size(241, 20);
            numericUpDown_sendingDelay.TabIndex = 12;
            numericUpDown_sendingDelay.Value = new Decimal(new int[4]
            {
        10,
        0,
        0,
        0
            });
            numericUpDown_sendingDelay.ValueChanged += new EventHandler(NumericUpDown_sendingDelay_ValueChanged);
            label3.AutoSize = true;
            label3.Location = new Point(4, 28);
            label3.Name = "label3";
            label3.Size = new Size(221, 13);
            label3.TabIndex = 11;
            label3.Text = "Задержка между нажатиями клавиш (мс):";
            numericUpDown_typingDelay.BackColor = Color.WhiteSmoke;
            numericUpDown_typingDelay.ForeColor = SystemColors.WindowText;
            numericUpDown_typingDelay.Location = new Point(7, 44);
            numericUpDown_typingDelay.Maximum = new Decimal(new int[4]
            {
        1000000000,
        0,
        0,
        0
            });
            numericUpDown_typingDelay.Minimum = new Decimal(new int[4]
            {
        1,
        0,
        0,
        0
            });
            numericUpDown_typingDelay.Name = "numericUpDown_typingDelay";
            numericUpDown_typingDelay.Size = new Size(241, 20);
            numericUpDown_typingDelay.TabIndex = 10;
            numericUpDown_typingDelay.Value = new Decimal(new int[4]
            {
        10,
        0,
        0,
        0
            });
            numericUpDown_typingDelay.ValueChanged += new EventHandler(NumericUpDown_typingDelay_ValueChanged);
            label_textsCount.AutoSize = true;
            label_textsCount.Location = new Point(168, 9);
            label_textsCount.Name = "label_textsCount";
            label_textsCount.Size = new Size(13, 13);
            label_textsCount.TabIndex = 9;
            label_textsCount.Text = "0";
            label1.AutoSize = true;
            label1.Location = new Point(30, 9);
            label1.Name = "label1";
            label1.Size = new Size(143, 13);
            label1.TabIndex = 8;
            label1.Text = "Сообщений в typerTexts.txt:";
            textBox_name.BackColor = Color.WhiteSmoke;
            textBox_name.Location = new Point(7, 121);
            textBox_name.Name = "textBox_name";
            textBox_name.Size = new Size(241, 20);
            textBox_name.TabIndex = 16;
            textBox_name.TextChanged += new EventHandler(TextBox_name_TextChanged);
            label2.AutoSize = true;
            label2.Location = new Point(4, 107);
            label2.Name = "label2";
            label2.Size = new Size(66, 13);
            label2.TabIndex = 17;
            label2.Text = "Обращение";
            comboBox_placement.BackColor = Color.WhiteSmoke;
            comboBox_placement.FormattingEnabled = true;
            comboBox_placement.Items.AddRange(new object[3]
            {
         "В начале",
         "В конце",
         "В начале и в конце (через :)"
            });
            comboBox_placement.Location = new Point(254, 121);
            comboBox_placement.Name = "comboBox_placement";
            comboBox_placement.Size = new Size(233, 21);
            comboBox_placement.TabIndex = 18;
            comboBox_placement.SelectedIndexChanged += new EventHandler(ComboBox_placement_SelectedIndexChanged);
            label5.AutoSize = true;
            label5.Location = new Point(251, 105);
            label5.Name = "label5";
            label5.Size = new Size(142, 13);
            label5.TabIndex = 19;
            label5.Text = "Расположение обращения";
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.WindowFrame;
            ClientSize = new Size(499, 180);
            Controls.Add((Control)label5);
            Controls.Add((Control)comboBox_placement);
            Controls.Add((Control)label2);
            Controls.Add((Control)textBox_name);
            Controls.Add((Control)richTextBox);
            Controls.Add((Control)button_launch);
            Controls.Add((Control)label4);
            Controls.Add((Control)numericUpDown_sendingDelay);
            Controls.Add((Control)label3);
            Controls.Add((Control)numericUpDown_typingDelay);
            Controls.Add((Control)label_textsCount);
            Controls.Add((Control)label1);
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            Name = "TyperForm";
            Text = "ISB :: Typer";
            FormClosed += new FormClosedEventHandler(TyperForm_FormClosed);
            numericUpDown_sendingDelay.EndInit();
            numericUpDown_typingDelay.EndInit();
            ResumeLayout(false);
            PerformLayout();
        }
    }
}
