﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP.Captcha.CapLib
{
    public class CaptchaException : Exception
    {
        public CaptchaException()
        {

        }
        public CaptchaException(string message)
            : base(message)
        {

        }
        public CaptchaException(string message, Exception inner)
            : base(message, inner)
        {

        }

    }
}
