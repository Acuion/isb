﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISP.Captcha.CapLib
{
    /// <summary>
    /// Язык
    /// </summary>
    public enum CaptchaLanguageEnum
    {
        /// <summary>
        /// По-умолчанию
        /// </summary>
        Default = 0,
        /// <summary>
        /// Русский
        /// </summary>
        Russian = 1,
        /// <summary>
        /// Английский
        /// </summary>
        English = 2
    }
}
