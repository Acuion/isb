﻿using ISP.Captcha.CapLib;
using ISP.Configs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using ISP.Forms;
using VkNet.Utils.AntiCaptcha;

namespace ISP.Captcha
{
    internal sealed class CaptchaSolver : ICaptchaSolver
    {
        public static readonly Queue<string> CaptchaManualIdsToSolve = new Queue<string>();
        private static readonly Dictionary<string, string> CaptchaManualAnswers = new Dictionary<string, string>();

        public void CaptchaIsFalse()
        {
            LogForm.PushToLog("Капча была распознанана неверно");
        }

        public string Solve(string url)
        {
            LogForm.PushToLog("Обработка капчи");

            string reqestId = new Random().Next().ToString();
            string filename = $"tmp\\{reqestId}.png";
            new WebClient().DownloadFile(url, filename);

            string host = "";
            string key = "";
            switch (ConfigController.AnticapConfig.SelectedMode)
            {
                case SelectedMode.Manual:
                    CaptchaManualIdsToSolve.Enqueue(reqestId);
                    MainForm.NotifyInTray("Капча поставлена в очередь на ручной ввод");
                    while (!CaptchaManualAnswers.ContainsKey(reqestId))
                        System.Threading.Thread.Sleep(1000);

                    string ans = CaptchaManualAnswers[reqestId];
                    CaptchaManualAnswers.Remove(reqestId);
                    return ans;
                case SelectedMode.Anticaptcha:
                    host = "https://anti-captcha.com";
                    key = ConfigController.AnticapConfig.AnticaptchaKey;
                    break;
                case SelectedMode.Rucaptcha:
                    host = "https://rucaptcha.com";
                    key = ConfigController.AnticapConfig.RucaptchaKey;
                    break;
            }
            CaptchaClient cc = new CaptchaClient(host, key);
            string capId = cc.UploadCaptchaFile(filename);
            File.Delete(filename);

            string answer = null;
            while (string.IsNullOrEmpty(answer))
            {
                System.Threading.Thread.Sleep(1000);
                try
                {
                    answer = cc.GetCaptcha(capId);
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("CAPCHA_NOT_READY"))
                        LogForm.PushToLog("Ошибка обработки капчи: " + ex.Message);
                }
            }

            return answer;
        }

        public static void SetManualAns(string captId, string ans)
        {
            CaptchaManualAnswers.Add(captId, ans);
        }
    }
}
