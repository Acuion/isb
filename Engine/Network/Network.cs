﻿using System.IO;
using System.Net;

namespace ISP.Engine.Network
{
    internal static class Network
    {
        public static string GET(string link)
        {
            return new StreamReader(((HttpWebRequest)WebRequest.Create(link)).GetResponse().GetResponseStream()).ReadToEnd();
        }
    }
}
