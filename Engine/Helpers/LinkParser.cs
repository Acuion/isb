﻿using ISP.Engine.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ISP.Forms;
using VkNet.Model.RequestParams;

namespace ISP.Engine.Helpers
{
    internal struct TargetData
    {
        public enum TypeOfTarget
        {
            Unknown,
            NotFound,
            PersonalMessage,
            Chat,
            Wall,
            Photo,
            Video,
            Topic
        }
        public readonly TypeOfTarget Type;
        public long? Id1, Id2;

        public TargetData(TypeOfTarget type, string id1, string id2)
        {
            Type = type;
            Id1 = long.Parse(id1);
            Id2 = long.Parse(id2);
        }

        public TargetData(TypeOfTarget type)
        {
            Type = type;
            Id1 = Id2 = null;
        }
    }

    internal static class LinkParser
    {
        private static readonly Dictionary<Account, Dictionary<string, long?>> ChatFromName, UserFromName;

        static LinkParser()
        {
            ChatFromName = new Dictionary<Account, Dictionary<string, long?>>();
            UserFromName = new Dictionary<Account, Dictionary<string, long?>>();
        }

        public static void ClearCache()
        {
            ChatFromName.Clear();
            UserFromName.Clear();
        }

        public static TargetData Parse(Account acc, string link)
        {
            var api = acc.VkApi;

            Match patternChat = new Regex("im\\?sel=c([0-9]+)").Match(link);
            Match patternPm = new Regex("im\\?sel=([0-9]+)").Match(link);
            Match patternWall = new Regex("wall([0-9]+)_([0-9]+)").Match(link);
            Match patternPhoto = new Regex("photo([0-9]+)_([0-9]+)").Match(link);
            Match patternVideo = new Regex("video([0-9]+)_([0-9]+)").Match(link);
            Match patternTopic = new Regex("topic([0-9]+)_([0-9]+)").Match(link);
            Match patternChatname = new Regex("chatname=(.+)").Match(link);
            Match patternUsername = new Regex("username=(.+)").Match(link);

            if (patternChat.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.Chat, patternChat.Groups[1].Value, "0");
            }
            if (patternPm.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.PersonalMessage, patternPm.Groups[1].Value, "0");
            }
            if (patternWall.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.Wall, patternWall.Groups[1].Value, patternWall.Groups[2].Value);
            }
            if (patternPhoto.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.Photo, patternPhoto.Groups[1].Value, patternPhoto.Groups[2].Value);
            }
            if (patternVideo.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.Video, patternVideo.Groups[1].Value, patternVideo.Groups[2].Value);
            }
            if (patternTopic.Success)
            {
                return new TargetData(TargetData.TypeOfTarget.Topic, patternTopic.Groups[1].Value, patternTopic.Groups[2].Value);
            }
            if (patternChatname.Success)
            {
                string nameToSearch = patternChatname.Groups[1].Value.ToLower();
                long? targetId = null;
                if (!ChatFromName.Keys.Contains(acc))
                    ChatFromName[acc] = new Dictionary<string, long?>();
                if (ChatFromName[acc].Keys.Contains(nameToSearch))
                    targetId = ChatFromName[acc][nameToSearch];
                else
                {
                    LogForm.PushToLog(acc, $"Поиск беседы: {nameToSearch}...");

                    int offset = 0;
                    while (targetId == null)
                    {
                        var resp = api.Messages.GetDialogs(new MessagesDialogsGetParams() { Count = 200, Offset = offset });
                        if (resp.Messages.Count == 0)
                            break;

                        var found = from msg in resp.Messages
                                    where msg.Title.ToLower() == nameToSearch
                                    select msg.ChatId;

                        targetId = found.DefaultIfEmpty(null).First();

                        offset += 200;
                    }

                    ChatFromName[acc][nameToSearch] = targetId;
                }

                if (targetId == null)
                {
                    LogForm.PushToLog(acc, $"Название беседы не найдено: {nameToSearch}");
                    return new TargetData(TargetData.TypeOfTarget.NotFound);
                }

                return new TargetData(TargetData.TypeOfTarget.Chat, targetId.ToString(), "0");
            }
            if (patternUsername.Success)
            {
                string nameToSearch = patternUsername.Groups[1].Value.ToLower();
                long? targetId = null;
                if (!UserFromName.Keys.Contains(acc))
                    UserFromName[acc] = new Dictionary<string, long?>();
                if (UserFromName[acc].Keys.Contains(nameToSearch))
                    targetId = UserFromName[acc][nameToSearch];
                else
                {
                    LogForm.PushToLog(acc, $"Поиск пользователя: {nameToSearch}...");

                    int offset = 0;
                    while (targetId == null)
                    {
                        var resp = api.Messages.GetDialogs(new MessagesDialogsGetParams() { Count = 200, Offset = offset });
                        if (resp.Messages.Count == 0)
                            break;

                        var found = from msg in resp.Messages
                                    where msg.UserId != null
                                    && new Func<string>(() =>
                                    {
                                        var info = api.Users.Get((long)msg.UserId);
                                        return info.FirstName + " " + info.LastName;
                                    }).Invoke().ToLower() == nameToSearch
                                    select msg.UserId;

                        targetId = found.DefaultIfEmpty(null).First();

                        offset += 200;
                    }

                    UserFromName[acc][nameToSearch] = targetId;
                }

                if (targetId == null)
                {
                    LogForm.PushToLog(acc, $"Пользователь не найден: {nameToSearch}");
                    return new TargetData(TargetData.TypeOfTarget.NotFound);
                }

                return new TargetData(TargetData.TypeOfTarget.PersonalMessage, targetId.ToString(), "0");
            }
            return new TargetData(TargetData.TypeOfTarget.Unknown);
        }
    }
}
