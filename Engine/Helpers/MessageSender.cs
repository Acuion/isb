﻿using ISP.Engine.Accounts;
using ISP.Forms;
using VkNet.Model.RequestParams;

namespace ISP.Engine.Helpers
{
    internal static class MessageSender
    {
        public static void SendVoiceMessage(Account from, TargetData to, string message)
        {
            MessagesSendParams msp = new MessagesSendParams();
            switch (to.Type)
            {
                case TargetData.TypeOfTarget.Chat:
                    msp.ChatId = to.Id1;
                    break;
                case TargetData.TypeOfTarget.PersonalMessage:
                    msp.UserId = to.Id1;
                    break;
                default:
                    LogForm.PushToLog(from, "Неверный формат цели голосового сообщения");
                    return;
            }

            var api = from.VkApi;
            
            msp.Attachments = from.FlooderTaskSettings.VoiceMessage(from, message);

            api.Messages.Send(msp);
        }
    }
}
