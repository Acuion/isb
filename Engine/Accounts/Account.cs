﻿using VkNet;
using ISP.Tasks.Settings;
using ISP.Captcha;
using System;
using Newtonsoft.Json;
using System.IO;
using ISP.Forms;
using ISP.Forms.Dialogs;
using VkNet.Model;

namespace ISP.Engine.Accounts
{
    internal sealed class Account
    {
        public VkApi VkApi { get; private set; }

        public FlooderTaskSettings FlooderTaskSettings;
        public ChatsTaskSettings ChatsTaskSettings;
        public VoiceTaskSettings VoiceTaskSettings;
        public InviteTaskSettings InviteTaskSettings;

        public string Login { get; set; }
        public User Myself { get; set; }
        public string Token { get; set; }

        private enum AuthResult
        {
            Ok,
            InvalidCred,
            Other
        };

        public Account()
        {
            FlooderTaskSettings = new FlooderTaskSettings();
            VoiceTaskSettings = new VoiceTaskSettings();
            ChatsTaskSettings = new ChatsTaskSettings();
            InviteTaskSettings = new InviteTaskSettings();
        }

        public void ValidateSettings()
        {
            if (FlooderTaskSettings == null)
                FlooderTaskSettings = new FlooderTaskSettings();
            if (VoiceTaskSettings == null)
                VoiceTaskSettings = new VoiceTaskSettings();
            if (ChatsTaskSettings == null)
                ChatsTaskSettings = new ChatsTaskSettings();
            if (InviteTaskSettings == null)
                InviteTaskSettings = new InviteTaskSettings();
            FlooderTaskSettings.Validate();
            ChatsTaskSettings.Validate();
            VoiceTaskSettings.Validate();
            InviteTaskSettings.Validate();
        }

        public void Reauth(CaptchaSolver solver)
        {
            VkApi = new VkApi(solver);
            ApiAuthParams aup = new ApiAuthParams()
            {
                AccessToken = Token
            };
            VkApi.Authorize(aup);
        }

        private AuthResult Auth(string login, string password, CaptchaSolver solver, Func<string> twoFactHandler, ulong appId)
        {
            VkApi = new VkApi(solver);

            ApiAuthParams aup = new ApiAuthParams()
            {
                ApplicationId = appId,
                Login = login,
                Password = password,
                Settings = VkNet.Enums.Filters.Settings.All | VkNet.Enums.Filters.Settings.Offline,
                TwoFactorAuthorization = twoFactHandler
            };

            try
            {
                VkApi.Authorize(aup);
                Token = VkApi.Token;
                return AuthResult.Ok;
            }
            catch (Exception ex)
            {
                VkApi = null;
                if (ex.Message.Contains("Invalid authorization"))
                    return AuthResult.InvalidCred;
                else
                    return AuthResult.Other;
            }
        }

        public void Auth(string login, string password, CaptchaSolver solver, Func<string> twoFactHandler)
        {
            Login = login;
            VkApi = null;

            try
            {
                switch (Auth(login, password, solver, null, 2168679))
                {
                    case AuthResult.InvalidCred:
                        LogForm.PushToLog(this, "Ошибка авторизации. Включена двуфакторная авторизация?");
                        TwoFactAsker.AccName = login;
                        Auth(login, password, solver, twoFactHandler, 2168679);
                        break;
                    case AuthResult.Other:
                        LogForm.PushToLog(this, "Неизвестная ошибка, попытка авторизоваться через другое приложение");
                        if (Auth(login, password, solver, null, 4747736) == AuthResult.InvalidCred)
                            Auth(login, password, solver, twoFactHandler, 4747736);
                        break;
                }

                if (VkApi != null)
                {
                    LogForm.PushToLog(this, "Успешно авторизован");
                    SaveToDisk();
                }
                else
                    LogForm.PushToLog(this, "Ошибка авторзиации");
            }
            catch (Exception ex)
            {
                LogForm.PushToLog(this, "Ошибка авторзиации: " + ex.Message);
                VkApi = null;
            }
        }

        public void ResetSettings()
        {
            FlooderTaskSettings = new FlooderTaskSettings();
            VoiceTaskSettings = new VoiceTaskSettings();
            ChatsTaskSettings = new ChatsTaskSettings();
            InviteTaskSettings = new InviteTaskSettings();
            //TODO: Вставлять сюда новые
        }

        public void SaveToDisk()
        {
            string json = JsonConvert.SerializeObject(this);
            File.WriteAllText($"Accounts\\{Login}.json", json);
        }
    }
}
