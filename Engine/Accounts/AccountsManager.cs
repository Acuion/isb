﻿using ISP.Engine.Helpers;
using ISP.Tasks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ISP.Forms;
using VkNet.Model;

namespace ISP.Engine.Accounts
{
    internal static class AccountsManager
    {
        private static readonly Dictionary<string, Account> Accounts;
        private static string _activeAccount;
        private static readonly List<ISBTask> AccsTasks;
        private static bool _toAll;

        static AccountsManager()
        {
            Accounts = new Dictionary<string, Account>();
            AccsTasks = new List<ISBTask>();
            _activeAccount = null;
            _toAll = false;
        }

        public static bool ToggleToAll()
        {
            _toAll = !_toAll;
            return _toAll;
        }

        public static void LoadAndPrepareAccounts(Func<string> twoFactHandler)
        {
            string[] data = File.ReadAllLines("Txts\\accounts.txt", Encoding.GetEncoding("windows-1251"));
            Captcha.CaptchaSolver solver = new Captcha.CaptchaSolver();
            foreach (string cred in data)
            {
                try
                {
                    string[] logandpass = cred.Split(':');
                    if (logandpass.Length != 2)
                        continue;
                    Account currAcc;
                    try
                    {
                        currAcc = JsonConvert.DeserializeObject<Account>(File.ReadAllText($"Accounts\\{logandpass[0]}.json"));
                    }
                    catch
                    {
                        currAcc = new Account();
                    }
                    bool relogin = false;
                    try
                    {
                        if (currAcc.Token == null)
                            relogin = true;
                        else
                        {
                            currAcc.Reauth(solver);
                            var rawUser = currAcc.VkApi.Call("users.get", VkNet.Utils.VkParameters.Empty)[0];
                            currAcc.Myself = new User() { FirstName = rawUser["first_name"], LastName = rawUser["last_name"], Id = rawUser["id"] };
                        }
                    }
                    catch
                    {
                        relogin = true;
                    }
                    if (relogin)
                    {
                        currAcc.Auth(logandpass[0], logandpass[1], solver, twoFactHandler);
                        var rawUser = currAcc.VkApi.Call("users.get", VkNet.Utils.VkParameters.Empty)[0];
                        currAcc.Myself = new User() { FirstName = rawUser["first_name"], LastName = rawUser["last_name"], Id = rawUser["id"] };
                        currAcc.SaveToDisk();
                    }
                    Accounts.Add(logandpass[0], currAcc);
                    if (Accounts.Last().Value.VkApi != null && _activeAccount == null)
                        _activeAccount = Accounts.Last().Key;
                }
                catch (Exception ex)
                {
                    LogForm.PushToLog($"Ошибка при загрузке одного из аккаунтов: {ex.Message}");
                }
            }
            Accounts.ToList().ForEach(x => x.Value.ValidateSettings());
        }

        public static List<string> GetAccountsList()
        {
            return (from acc in Accounts
                    where acc.Value.VkApi != null
                    select acc.Key + $" ({acc.Value.Myself.FirstName} {acc.Value.Myself.LastName})"
                    ).ToList();
        }

        public static void SetActiveAccount(string accWithName)
        {
            _activeAccount = accWithName.Split(' ')[0];
        }

        public static Account ActiveAccount => Accounts[_activeAccount];

        public static Account GetAccount(string acc)
        {
            return Accounts[acc.Split(' ')[0]];
        }

        public static void ApplyToCurrent(Action<Account> func)
        {
            if (_toAll)
            {
                Accounts.Values.ToList().ForEach(func);
            }
            else
            {
                func(ActiveAccount);
            }
        }

        public static void StartAllTasks()
        {
            LinkParser.ClearCache();
            AccsTasks.Clear();
            foreach (Account acc in Accounts.Values)
            {
                void AddTask(ISBTask task)
                {
                    AccsTasks.Add(task);
                    task.LaunchTask(acc);
                }

                AddTask(new FlooderTask());
                AddTask(new ChatsTask());
                AddTask(new InviteTask());
            }
        }

        public static void StopAllTasks()
        {
            foreach (ISBTask task in AccsTasks)
                task.StopTask();
        }
    }
}
