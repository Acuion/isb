﻿using ISP.Engine.Accounts;
using ISP.Engine.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ISP.Forms;

namespace ISP.Tasks.Settings
{
    internal sealed class ChatsTarget
    {
        public string Link, TitleMode, Title, AvatarMode;

        [JsonIgnore]
        public string TargetPhoto200 = null;

        public bool FloodWithAvatar;

        public ChatsTarget() { }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ChatsTarget ftg2 = obj as ChatsTarget;
            if (ftg2 == null) return false;
            return Equals(ftg2);
        }

        private bool Equals(ChatsTarget other)
        {
            return string.Equals(Link, other.Link)
                && string.Equals(TitleMode, other.TitleMode)
                && string.Equals(Title, other.Title)
                && string.Equals(AvatarMode, other.AvatarMode)
                && string.Equals(TargetPhoto200, other.TargetPhoto200)
                && FloodWithAvatar == other.FloodWithAvatar;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Link != null ? Link.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TitleMode != null ? TitleMode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Title != null ? Title.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (AvatarMode != null ? AvatarMode.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (TargetPhoto200 != null ? TargetPhoto200.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ FloodWithAvatar.GetHashCode();
                return hashCode;
            }
        }
    }

    internal sealed class ChatsTaskSettings : ISBTaskSettings
    {
        private class CounterContainer
        {
            public int TitleIndex = -1;
        }

        private static Dictionary<TargetData, CounterContainer> _targetRelatedCounters;

        public readonly List<ChatsTarget> Targets;
        private static List<string> _titlesToFlood;

        static ChatsTaskSettings()
        {
            ReloadFromTxts();
        }

        public static void ReloadFromTxts()
        {
            _titlesToFlood = new List<string>(File.ReadAllLines("Txts\\titles.txt", Encoding.GetEncoding("windows-1251")));
            _targetRelatedCounters = new Dictionary<TargetData, CounterContainer>();
        }

        public ChatsTaskSettings()
        {
            Targets = new List<ChatsTarget>();
        }

        private static CounterContainer CounterContainerFromTarget(Account acc, ChatsTarget ctg)
        {
            var td = LinkParser.Parse(acc, ctg.Link);
            if (!_targetRelatedCounters.Keys.Contains(td))
                _targetRelatedCounters[td] = new CounterContainer();
            return _targetRelatedCounters[td];
        }

        public void AddNewTarget(ChatsTarget target)
        {
            lock (Targets)
            {
                Targets.Add(target);
            }
        }

        public void RemoveTarget(ChatsTarget target)
        {
            lock (Targets)
            {
                Targets.Remove(target);
            }
        }

        public void ReplaceTarget(ChatsTarget from, ChatsTarget to)
        {
            lock (Targets)
            {
                int pos = Targets.IndexOf(from);
                if (pos != -1)
                    Targets[pos] = to;
            }
        }

        public void ParseDataGridView(DataGridView view)
        {
            lock (Targets)
            {
                Targets.Clear();

                foreach (DataGridViewRow row in view.Rows)
                {
                    Targets.Add(new ChatsTarget()
                    {
                        Link = (row.Cells[0].Value ?? "").ToString(),
                        TitleMode = row.Cells[1].Value.ToString(),
                        Title = (row.Cells[2].Value ?? "").ToString(),
                        AvatarMode = row.Cells[3].Value.ToString(),
                        FloodWithAvatar = (bool)row.Cells[4].Value
                    });
                }
            }
        }

        public override void Validate()
        {
            var avs = new List<string>(Directory.GetFiles("Upload\\Avatars"));
            avs = avs.ConvertAll(Path.GetFileName);
            foreach (var target in Targets)
                if (target.AvatarMode != "Ничего не делать" &&
                    !avs.Contains(target.AvatarMode))
                    target.AvatarMode = "Ничего не делать";
        }

        public string GetTitle(Account acc, ChatsTarget ctg)
        {
            if (_titlesToFlood.Count == 0)
            {
                LogForm.PushToLog("Нет доступных названий бесед для флуда");
                return null;
            }

            var ccft = CounterContainerFromTarget(acc, ctg);
            ccft.TitleIndex = (ccft.TitleIndex + 1) % _titlesToFlood.Count;

            return _titlesToFlood[ccft.TitleIndex];
        }
    }
}
