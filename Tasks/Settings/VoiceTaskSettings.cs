﻿using ISP.Misc;

namespace ISP.Tasks.Settings
{
    internal sealed class VoiceTaskSettings : ISBTaskSettings
    {
        public string Voice = "zahar";
        public string Emotion = "neutral";
        public int Speed = 10;
        public string Yandexapi;
        public string Message;
        public string Target;

        public override void Validate()
        {
            Speed = Speed.Cut(1, 30);
        }
    }
}
