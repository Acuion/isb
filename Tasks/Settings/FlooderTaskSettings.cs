﻿using ISP.Engine.Accounts;
using ISP.Engine.Helpers;
using ISP.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ISP.Forms;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Utils;

namespace ISP.Tasks.Settings
{
    internal sealed class FlooderTarget
    {
        public string Link;
        public string NamePlace;
        public string Name;
        public string Contains;

        public FlooderTarget() { }

        public FlooderTarget(FlooderTarget ftg)
        {
            Link = ftg.Link;
            NamePlace = ftg.NamePlace;
            Name = ftg.Name;
            Contains = ftg.Contains;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            FlooderTarget ftg2 = obj as FlooderTarget;
            if (ftg2 == null) return false;
            return Equals(ftg2);
        }

        private bool Equals(FlooderTarget other)
        {
            return string.Equals(Link, other.Link)
                && string.Equals(NamePlace, other.NamePlace)
                && string.Equals(Name, other.Name)
                && string.Equals(Contains, other.Contains);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Link != null ? Link.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (NamePlace != null ? NamePlace.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Contains != null ? Contains.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    internal sealed class FlooderTaskSettings : ISBTaskSettings
    {
        private class CounterContainer
        {
            public int PhIndex = -1, PhDotsIndex = -1, StIndex = -1;
        }

        public readonly List<FlooderTarget> Targets;
        public bool ImagesFromFolder;
        public string PhrasesFile, PhrasesWithDotsFile, StickersFile, StickerId;

        private static Dictionary<TargetData, CounterContainer> _targetRelatedCounters;
        private List<string> _phrases, _phrasesWithDots;
        private static List<MediaAttachment> _audios, _videos;
        private List<MediaAttachment> _documents, _images, _stickers;
        private static Random _rnd;

        static FlooderTaskSettings()
        {
            ReloadFromTxts();
        }

        public static void ReloadFromTxts()
        {
            _targetRelatedCounters = new Dictionary<TargetData, CounterContainer>();
            _rnd = new Random();
            _audios = new List<MediaAttachment>();
            _videos = new List<MediaAttachment>();

            Regex audiosPattren = new Regex("audio_?(-?[0-9]+)_([0-9]+)");
            Regex videosPattren = new Regex("video(-?[0-9]+)_([0-9]+)");

            foreach (string video in System.IO.File.ReadAllLines("Txts\\Attachments\\videos.txt", Encoding.GetEncoding("windows-1251")))
            {
                Match m = videosPattren.Match(video);
                if (m.Success)
                {
                    _videos.Add(new Video()
                    {
                        OwnerId = long.Parse(m.Groups[1].Value),
                        Id = long.Parse(m.Groups[2].Value)
                    });
                }
                else
                {
                    LogForm.PushToLog($"{video} - некоррекный формат видео");
                }
            }
            foreach (string audio in System.IO.File.ReadAllLines("Txts\\Attachments\\audios.txt", Encoding.GetEncoding("windows-1251")))
            {
                Match m = audiosPattren.Match(audio);
                if (m.Success)
                {
                    _audios.Add(new Audio()
                    {
                        OwnerId = long.Parse(m.Groups[1].Value),
                        Id = long.Parse(m.Groups[2].Value)
                    });
                }
                else
                {
                    LogForm.PushToLog($"{audio} - некоррекный формат аудио");
                }
            }
        }

        public void LoadAccountSpecific(Account acc)
        {
            _phrases = new List<string>();
            _phrasesWithDots = new List<string>();
            if (!string.IsNullOrEmpty(PhrasesFile))
                _phrases = new List<string>(System.IO.File.ReadAllLines($"Txts\\Phrases\\Simple\\{PhrasesFile}", Encoding.GetEncoding("windows-1251")));
            if (!string.IsNullOrEmpty(PhrasesWithDotsFile))
                _phrasesWithDots = new List<string>(System.IO.File.ReadAllLines($"Txts\\Phrases\\Dots\\{PhrasesWithDotsFile}", Encoding.GetEncoding("windows-1251")));

            _documents = new List<MediaAttachment>();
            _images = new List<MediaAttachment>();
            _stickers = new List<MediaAttachment>();

            Regex imagesPattren = new Regex("photo(-?[0-9]+)_([0-9]+)");
            Regex stickersPattren = new Regex("([0-9]+)");

            if (!string.IsNullOrEmpty(StickersFile))
                foreach (string sticker in System.IO.File.ReadAllLines($"Txts\\Stickers\\{StickersFile}", Encoding.GetEncoding("windows-1251")))
                {
                    Match m = stickersPattren.Match(sticker);
                    if (m.Success)
                    {
                        _stickers.Add(new Sticker()
                        {
                            Id = long.Parse(m.Groups[1].Value)
                        });
                    }
                    else
                    {
                        LogForm.PushToLog($"{sticker} - некоррекный формат стикера");
                    }
                }

            UploadDocuments(acc);

            if (ImagesFromFolder)
                UploadImages(acc);
            else
                foreach (string image in System.IO.File.ReadAllLines("Txts\\Attachments\\images.txt", Encoding.GetEncoding("windows-1251")))
                {
                    Match m = imagesPattren.Match(image);
                    if (m.Success)
                    {
                        _images.Add(new Photo()
                        {
                            OwnerId = long.Parse(m.Groups[1].Value),
                            Id = long.Parse(m.Groups[2].Value)
                        });
                    }
                    else
                    {
                        LogForm.PushToLog($"{image} - некоррекный формат изображения");
                    }
                }
        }

        public void AddNewTarget(FlooderTarget target)
        {
            lock (Targets)
            {
                Targets.Add(target);
            }
        }

        public void RemoveTarget(FlooderTarget target)
        {
            lock (Targets)
            {
                Targets.Remove(target);
            }
        }

        public void ReplaceTarget(FlooderTarget from, FlooderTarget to)
        {
            lock (Targets)
            {
                int pos = Targets.IndexOf(from);
                if (pos != -1)
                    Targets[pos] = to;
            }
        }

        public FlooderTaskSettings()
        {
            Targets = new List<FlooderTarget>();
            ImagesFromFolder = false;
            PhrasesFile = PhrasesWithDotsFile = StickersFile = null;
        }

        public override void Validate()
        {
            var txtss = new List<string>(Directory.GetFiles("Txts\\Phrases\\Simple"));
            txtss = txtss.ConvertAll(Path.GetFileName);
            var txtsd = new List<string>(Directory.GetFiles("Txts\\Phrases\\Dots"));
            txtsd = txtsd.ConvertAll(Path.GetFileName);
            var stks = new List<string>(Directory.GetFiles("Txts\\Stickers"));
            stks = stks.ConvertAll(Path.GetFileName);
            if (!txtss.Contains(PhrasesFile))
                PhrasesFile = null;
            if (!txtsd.Contains(PhrasesWithDotsFile))
                PhrasesWithDotsFile = null;
            if (!stks.Contains(StickersFile))
            {
                StickersFile = null;
                StickerId = null;
            }
        }

        public void ParseDataGridView(DataGridView view)
        {
            lock (Targets)
            {
                Targets.Clear();

                foreach (DataGridViewRow row in view.Rows)
                {
                    Targets.Add(new FlooderTarget()
                    {
                        Link = (row.Cells[0].Value ?? "").ToString(),
                        NamePlace = row.Cells[1].Value.ToString(),
                        Name = (row.Cells[2].Value ?? "").ToString(),
                        Contains = row.Cells[3].Value.ToString()
                    });
                }
            }
        }

        private void UploadDocuments(Account acc)
        {
            LogForm.PushToLog(acc, "[Флудер]: загрузка документов...");
            var api = acc.VkApi;
            var docFiles = Directory.GetFiles("Upload\\Documents");
            var wc = new System.Net.WebClient();

            foreach (string path in docFiles)
            {
                var uploadServer = api.Docs.GetUploadServer();
                var responseFile = Encoding.ASCII.GetString(wc.UploadFile(uploadServer.UploadUrl, path));
                _documents.AddRange(api.Docs.Save(responseFile, "gif"));
                LogForm.PushToLog(acc, $"[Флудер]: загрузка {path} завершена");
            }
        }

        private void UploadImages(Account acc)
        {
            LogForm.PushToLog(acc, "[Флудер]: загрузка изображений...");
            var api = acc.VkApi;
            var imageFiles = Directory.GetFiles("Upload\\Images");
            var wc = new System.Net.WebClient();

            foreach (string path in imageFiles)
            {
                var uploadServer = api.Photo.GetWallUploadServer();
                var responseFile = Encoding.ASCII.GetString(wc.UploadFile(uploadServer.UploadUrl, path));
                _images.AddRange(api.Photo.SaveWallPhoto(responseFile));
                LogForm.PushToLog(acc, $"[Флудер]: загрузка {path} завершена");
            }
        }

        private static CounterContainer CounterContainerFromTarget(Account acc, FlooderTarget ft)
        {
            var td = LinkParser.Parse(acc, ft.Link);
            if (!_targetRelatedCounters.Keys.Contains(td))
                _targetRelatedCounters[td] = new CounterContainer();
            return _targetRelatedCounters[td];
        }

        public IReadOnlyCollection<MediaAttachment> VoiceMessage(Account acc, string message)
        {
            try
            {
                var vts = acc.VoiceTaskSettings;
                var api = acc.VkApi;

                var wc = new System.Net.WebClient();
                string sp = (vts.Speed * 0.1).ToString().Replace(",", ".");
                string filename = $"tmp\\{_rnd.Next()}.mp3";
                message = WebUtility.UrlEncode(message);
                string link = $"https://tts.voicetech.yandex.net/generate?text={message}&format=mp3&lang=ru-RU&speaker={vts.Voice}&emotion={vts.Emotion}&speed={sp}&key={vts.Yandexapi}";
                wc.DownloadFile(link, filename);

                //UPLOAD

                VkParameters upl = new VkParameters
                {
                    { "type", "audio_message" }
                };
                UploadServerInfo uploadServerInfo = api.Call("docs.getUploadServer", upl);
                string uplData = Encoding.UTF8.GetString(wc.UploadFile(uploadServerInfo.UploadUrl, filename));
                string tg = StrWrk.GetBetween(uplData, "\"file\":\"", "\"}");
                VkParameters sve = new VkParameters
            {
                { "file", tg }
            };
                var readyToSend = api.Call("docs.save", sve).ToReadOnlyCollectionOf<Document>(r => r);

                System.IO.File.Delete(filename);

                return readyToSend;
            }
            catch (Exception ex)
            {
                LogForm.PushToLog(acc, "Ошибка при отправке голосового сообщения (что-то не так с api ключом?):" + ex.Message);
                return null;
            }
        }//TODO: вынести во что-то обобщённое

        public IReadOnlyCollection<MediaAttachment> RandomImage()
        {
            if (_images.Count == 0)
            {
                LogForm.PushToLog("Нет доступных изображений для флуда");
                return null;
            }

            var toRet = new List<MediaAttachment>
            {
                _images[_rnd.Next(0, _images.Count)]
            };
            return toRet.ToReadOnlyCollection();
        }

        public IReadOnlyCollection<MediaAttachment> RandomSticker(Account acc, FlooderTarget ft)
        {
            if (_stickers.Count == 0)
            {
                LogForm.PushToLog("Нет доступных стикеров для флуда");
                return null;
            }

            List<MediaAttachment> toRet;
            if (StickerId == "По порядку")
            {
                var ccft = CounterContainerFromTarget(acc, ft);
                ccft.StIndex = (ccft.StIndex + 1) % _stickers.Count;

                toRet = new List<MediaAttachment>
                {
                    _stickers[CounterContainerFromTarget(acc, ft).StIndex]
                };
            }
            else
            {
                try
                {
                    toRet = new List<MediaAttachment>
                    {
                        new Sticker()
                        {
                            Id = long.Parse(new Regex("([0-9]+)").Match(StickerId).Groups[1].Value)
                        }
                    };
                }
                catch
                {
                    LogForm.PushToLog($"{StickerId} - неверный формат стикера");
                    return null;
                }
            }
            return toRet.ToReadOnlyCollection();
        }

        public IReadOnlyCollection<MediaAttachment> RandomVideo()
        {
            if (_videos.Count == 0)
            {
                LogForm.PushToLog("videos.txt пуст");
                return null;
            }

            var toRet = new List<MediaAttachment>
            {
                _videos[_rnd.Next(0, _videos.Count)]
            };
            return toRet.ToReadOnlyCollection();
        }

        public IReadOnlyCollection<MediaAttachment> RandomDocument()
        {
            if (_documents.Count == 0)
            {
                LogForm.PushToLog("Папка с документами пуста");
                return null;
            }

            var toRet = new List<MediaAttachment>
            {
                _documents[_rnd.Next(0, _documents.Count)]
            };
            return toRet.ToReadOnlyCollection();
        }

        public IReadOnlyCollection<MediaAttachment> RandomAudio()
        {
            if (_audios.Count == 0)
            {
                LogForm.PushToLog("audios.txt пуст");
                return null;
            }

            var toRet = new List<MediaAttachment>
            {
                _audios[_rnd.Next(0, _audios.Count)]
            };
            return toRet.ToReadOnlyCollection();
        }

        public string GetPhrase(Account acc, FlooderTarget ft)
        {
            if (_phrases.Count == 0)
            {
                LogForm.PushToLog("Нет доступных фраз для флуда");
                return null;
            }

            var ccft = CounterContainerFromTarget(acc, ft);
            ccft.PhIndex = (ccft.PhIndex + 1) % _phrases.Count;

            return _phrases[ccft.PhIndex];
        }

        public string GetPhraseWithDots(Account acc, FlooderTarget ft)
        {
            if (_phrasesWithDots.Count == 0)
            {
                LogForm.PushToLog("Нет доступных фраз с точками для флуда");
                return null;
            }

            var ccft = CounterContainerFromTarget(acc, ft);
            ccft.PhDotsIndex = (ccft.PhDotsIndex + 1) % _phrasesWithDots.Count;

            return _phrasesWithDots[ccft.PhDotsIndex++];
        }
    }
}
