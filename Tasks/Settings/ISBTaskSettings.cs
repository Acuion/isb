﻿namespace ISP.Tasks.Settings
{
    public abstract class ISBTaskSettings
    {
        public bool Enabled = false;
        public int Delay = 333;

        public abstract void Validate();
    }
}
