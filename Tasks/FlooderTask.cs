﻿using ISP.Engine.Accounts;
using ISP.Engine.Helpers;
using ISP.Engine.Network;
using ISP.Misc;
using ISP.Tasks.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ISP.Configs;
using ISP.Forms;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;

namespace ISP.Tasks
{
    internal sealed class FlooderTask : ISBTask
    {
        private string BuildFromDamn(FlooderTarget to)
        {
            string resp = Network.GET($"https://damn.ru/?name={to.Name}&sex=m");
            resp = StrWrk.GetBetween(resp, "<div class=\"damn\" ", "</div>").Replace("<span class=\"name\">", "").Replace("</span>", "");
            return StrWrk.QSubstr(resp, ">").Replace("&mdash;", "--");//TODO: получше парсинг
        }

        private string BuildRandomMessage(Account acc, FlooderTaskSettings fts, FlooderTarget to)
        {
            string msg = to.NamePlace != "Вместо ..." ? fts.GetPhrase(acc, to) : fts.GetPhraseWithDots(acc, to);

            switch (to.NamePlace)
            {
                case "Начало":
                    msg = to.Name + msg;
                    break;
                case "Конец":
                    msg = msg + to.Name;
                    break;
                case "Вместо ...":
                    msg = msg.Replace("...", to.Name);
                    break;
                case "В начале и в конце (через :)":
                    {
                        string[] twon = to.Name.Split(':');
                        msg = twon[0] + msg + (twon.Length == 1 ? twon[0] : twon[1]);
                    }
                    break;
            }

            return msg;
        }

        private void SendFlood(Account acc, FlooderTarget ftg)
        {
            var api = acc.VkApi;
            var targetData = LinkParser.Parse(acc, ftg.Link);
            var fts = acc.FlooderTaskSettings;

            List<MediaAttachment> attachments = new List<MediaAttachment>();
            string message = "";
            long stickerId = -1;

            switch (ftg.Contains)
            {
                case "Текст":
                    message = BuildRandomMessage(acc, fts, ftg);
                    break;
                case "Изображение":
                    message = ftg.Name;
                    attachments.AddRange(fts.RandomImage());
                    break;
                case "Изображение+текст":
                    attachments.AddRange(fts.RandomImage());
                    message = BuildRandomMessage(acc, fts, ftg);
                    break;
                case "Голосовое сообщение":
                    attachments.AddRange(fts.VoiceMessage(acc, BuildRandomMessage(acc, fts, ftg)));
                    break;
                case "Стикер":
                    stickerId = (long)fts.RandomSticker(acc, ftg).Last().Id;
                    break;
                case "Текст+стикер":
                    {
                        FlooderTarget ftgtxt = new FlooderTarget(ftg)
                        {
                            Contains = "Текст"
                        };
                        SendFlood(acc, ftgtxt);
                        FlooderTarget ftgstc = new FlooderTarget(ftg)
                        {
                            Contains = "Стикер"
                        };
                        SendFlood(acc, ftgstc);
                    }
                    return;
                case "Видеозапись":
                    attachments.AddRange(fts.RandomVideo());
                    break;
                case "Видеозапись+текст":
                    attachments.AddRange(fts.RandomVideo());
                    message = BuildRandomMessage(acc, fts, ftg);
                    break;
                case "Аудиозапись+текст":
                    attachments.AddRange(fts.RandomAudio());
                    message = BuildRandomMessage(acc, fts, ftg);
                    break;
                case "Аудиозапись+текст+картинка":
                    attachments.AddRange(fts.RandomAudio());
                    message = BuildRandomMessage(acc, fts, ftg);
                    attachments.AddRange(fts.RandomImage());
                    break;
                case "Аудиозапись+картинка":
                    attachments.AddRange(fts.RandomAudio());
                    attachments.AddRange(fts.RandomImage());
                    break;
                case "Документ+текст":
                    attachments.AddRange(fts.RandomDocument());
                    message = BuildRandomMessage(acc, fts, ftg);
                    break;
                case "damn.ru":
                    message = BuildFromDamn(ftg);
                    break;
            }

            switch (targetData.Type)
            {
                case TargetData.TypeOfTarget.Chat:
                    {
                        if (ConfigController.AntikickConfig.LeavedIntentionallyFrom != null
                            && ConfigController.AntikickConfig.LeavedIntentionallyFrom.ContainsKey(acc.Login)
                                && ConfigController.AntikickConfig.LeavedIntentionallyFrom[acc.Login]
                                .Contains((long)targetData.Id1))
                        break;
                        MessagesSendParams msp = new MessagesSendParams()
                        {
                            ChatId = targetData.Id1,
                            Message = message,
                            Attachments = attachments,
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Messages.Send(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.PersonalMessage:
                    {
                        MessagesSendParams msp = new MessagesSendParams()
                        {
                            UserId = targetData.Id1,
                            Message = message,
                            Attachments = attachments
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Messages.Send(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.Photo:
                    {
                        PhotoCreateCommentParams msp = new PhotoCreateCommentParams()
                        {
                            OwnerId = targetData.Id1,
                            PhotoId = (ulong)targetData.Id2,
                            Message = message,
                            Attachments = attachments
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Photo.CreateComment(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.Topic:
                    {
                        BoardCreateCommentParams msp = new BoardCreateCommentParams()
                        {
                            GroupId = targetData.Id1,
                            TopicId = (long)targetData.Id2,
                            Message = message,
                            Attachments = attachments
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Board.СreateComment(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.Video:
                    {
                        VideoCreateCommentParams msp = new VideoCreateCommentParams()
                        {
                            OwnerId = targetData.Id1,
                            VideoId = (long)targetData.Id2,
                            Message = message,
                            Attachments = attachments
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Video.CreateComment(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.Wall:
                    {
                        WallAddCommentParams msp = new WallAddCommentParams()
                        {
                            OwnerId = targetData.Id1,
                            PostId = (long)targetData.Id2,
                            Text = message,
                            Attachments = attachments
                        };
                        if (stickerId != -1)
                            msp.StickerId = (uint)stickerId;
                        api.Wall.AddComment(msp);
                    }
                    break;
                case TargetData.TypeOfTarget.NotFound:
                    break;
                default:
                    LogForm.PushToLog(acc, $"[Флудер]: \"{ftg.Link}\" - неподдерживаемый формат ссылки");
                    break;
            }
        }

        private void AsyncWorker(Account acc)
        {
            var fts = acc.FlooderTaskSettings;
            fts.LoadAccountSpecific(acc);
            var targets = fts.Targets;

            int targetIter = -1;

            void DoFlood()
            {
                if (!fts.Enabled)
                {
                    PeriodicTimer?.Dispose();
                    PeriodicTimer = null;
                    return;
                }

                lock (targets)
                {
                    targetIter = (targetIter + 1) % targets.Count;
                    try
                    {
                        FlooderTarget ftg = targets[targetIter];
                        SendFlood(acc, ftg);
                    }
                    catch (Exception ex)
                    {
                        LogForm.PushToLog(acc, "[Флудер]: " + ex.Message);
                    }
                }
            }

            PeriodicTimer = new SingleSimrunTimer(DoFlood, fts.Delay);
        }

        public override void LaunchTask(Account acc)
        {
            if (!acc.FlooderTaskSettings.Enabled)
                return;
            if (acc.FlooderTaskSettings.Targets.Count == 0)
            {
                LogForm.PushToLog(acc, "[Флудер]: ошибка запуска - отсутствуют цели");
                return;
            }

            new Task(() => AsyncWorker(acc)).Start();
        }

        public override void StopTask()
        {
            PeriodicTimer?.Dispose();
            PeriodicTimer = null;
        }
    }
}
